$(function(){
  var table=$("#data").DataTable({
    responsive: true,
    autoWidth: false,
    destroy: true,
    deferRender: true,
    ajax: {
      url: window.location.pathname,
      type: "POST",
      data: {
        action: "searchall",
        
      },
      dataSrc: "",
      
    },
    columns: [
      { data: "id" },
      { data: "clientes" },
      { data: "fecha" },
      { data: "tipo_inmueble" },
      { data: "detalle_mantenimiento" },
      { data: "fecha_ultimo" },
      { data: "fecha_mantenimiento" },
      {"defaultContent": '<button type="button" class="btnver btn-primary btn-success btn-flat btn" id="btnver" name="btnver" >Ver</button> <button type="button" class="btnnuevo btn-primary btn-success btn-flat btn" id="btnnuevo" name="btnnuevo">Nuevo</button>'
      },        
      
    ],
    columnDefs: [
      {
        targets: 2,

           render: DataTable.render.datetime('DD/MM/YYYY'),
            

                   
      },
      {
        targets: 5,

           render: DataTable.render.datetime('DD/MM/YYYY'),
            

                   
      },
      {
        targets: 6,

        render: DataTable.render.datetime('DD/MM/YYYY'),
      },
      
    ],
    
    initComplete: function (settings, json) {
    
    },
  
  });
  
 optener_novedad('#data tbody',table) 




});
    
optener_novedad=function(tbody,table){
  $(tbody).on("click","button.btnnuevo",function(){
    var data=table.row($(this).parents("tr")).data();
     $('input[name="action"]').val('add');
     $('input[name="id"]').val(data.id);
     $('#fichaModal').modal('show');
  });
  $(tbody).on("click","button.btnver",function(){
    var data=table.row($(this).parents("tr")).data();
    location.assign('fichamodal/'+data.id);
   
   
     
      
   
  });
  

}


  

   

 

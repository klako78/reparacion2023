$(function(){
  $("#data").DataTable({
    responsive: true,
    autoWidth: false,
    destroy: true,
    deferRender: true,
    ajax: {
      url: window.location.pathname,
      type: "POST",
      data: {
        action: "searchall",
        
      },
      dataSrc: "",
    },
    columns: [
      { data: "id" },
      { data: "novedad" },
      { data: "estado_novedad" },
      { data: "fecha_novedad" },
      { data: "usuario" },
      
    ],
    columnDefs: [
      {
       

                   
      },

      
    ],
    
    initComplete: function (settings, json) {
    
    },
 
  });

});
  

$('input[name="search"]').autocomplete({
    source: function (request, response) {
      $.ajax({
        url: window.location.pathname,
        type: "POST",
        data: {
          action: "autocomplete",
          term: request.term,
        },
        dataType: "json",
      })
        .done(function (data) {
          response(data);
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ": " + errorThrown);
        })
        .always(function (data) {});
    },
    delay: 100,
    minLength: 3,
    select: function (event, ui) {
      
     
      $('input[name="ced"]').val(ui.item.id);  
      
      $("#data").DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
          url: window.location.pathname,
          type: "POST",
          data: {
            action: "searchdata",
            id:   ui.item.id,
          },
          dataSrc: "",
        },
        columns: [
          { data: "id" },
          { data: "novedad" },
          { data: "estado_novedad" },
          { data: "fecha_novedad" },
          { data: "usuario" },
          
        ],
        columnDefs: [
          {
           
            targets: 3,
           
            render: DataTable.render.datetime('DD/MM/YYYY'),
                       
          },

          
        ],
        
        initComplete: function (settings, json) {
        
        },
     
      });
      
    },

   
});



    


 

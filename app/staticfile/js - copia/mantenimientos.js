$(function(){
  var table=$("#data").DataTable({
    responsive: true,
    autoWidth: false,
    destroy: true,
    deferRender: true,
    ajax: {
      url: window.location.pathname,
      type: "POST",
      data: {
        action: "searchall",
        
      },
      dataSrc: "",
    },
    columns: [
      { data: "id" },
      { data: "descripcion_actividad" },
      { data: "usuario" },
      { data: "estado_mantenimiento" },
      { data: "tipo_mantenimiento" },
      { data: "fecha_inicio" },
      { data: "fecha_terminacion" },
      { data: "inmueble"},
      {"defaultContent": '<button type="button" class="btnver btn-primary btn-success btn-flat btn" id="btnver" name="btnver" >Ver</button> <button type="button" class="btndetalle btn-primary btn-success btn-flat btn" id="btndetalle" name="btndetalle">Nuevo</button>'
      },        
      
    ],
    columnDefs: [
      {
        targets: 5,
           
            render: DataTable.render.datetime('DD/MM/YYYY'),
            

                   
      },
      {
        targets: 6,
           
            render: DataTable.render.datetime('DD/MM/YYYY'),
            

                   
      },
      {
        targets: 7,
           
          visible: false, 
            

                   
      },
    ],
    
    initComplete: function (settings, json) {
    
    },
  
  });
  
 optener_novedad('#data tbody',table) 

$('input[name="search"]').autocomplete({
    source: function (request, response) {
     
      $.ajax({
        url: window.location.pathname,
        type: "POST",
        data: {
          action: "autocomplete",
          term: request.term,
        },
        dataType: "json",
      })
        .done(function (data) {
          response(data);
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ": " + errorThrown);
        })
        .always(function (data) {});
    },
    delay: 100,
    minLength: 3,
    select: function (event, ui) {
      
     
      $('input[name="ced"]').val(ui.item.id);  
      
      var table=$("#data").DataTable({
        responsive: true,
        autoWidth: true,
        destroy: true,
        deferRender: true,
        ajax: {
          url: window.location.pathname,
          type: "POST",
          data: {
            action: "searchdata",
            id:   ui.item.id,
          },
          dataSrc: "",
        },
        columns: [
          { data: "id" },
          { data: "descripcion_actividad" },
          { data: "usuario" },
          { data: "estado_mantenimiento" },
          { data: "tipo_mantenimiento" },
          { data: "fecha_inicio" },
          { data: "fecha_terminacion" },
          {"defaultContent": '<button type="button" class="btnver btn-primary btn-success btn-flat btn" id="btnver" name="btnver" >Ver</button> <button type="button" class="btndetalle btn-primary btn-success btn-flat btn" id="btndetalle" name="btndetalle">Nuevo</button>'
          },        
        ],
        columnDefs: [
          {
            targets: 5,

               render: DataTable.render.datetime('DD/MM/YYYY'),
          },
          {
            targets: 6,

            render: DataTable.render.datetime('DD/MM/YYYY'),
          },
          
        ],
        
        initComplete: function (settings, json) {
           
        },
        
        
        
      });
      optener_novedad('#data tbody',table) 
      
    },

});


});
    
optener_novedad=function(tbody,table){
  $(tbody).on("click","button.btndetalle",function(){
    var data=table.row($(this).parents("tr")).data();
    $('input[name="cedula"]').val(data.inmueble);
     $('input[name="action"]').val('add');
     $('input[name="id_mantenimiento"]').val(data.id);
     $('input[name="novedad"]').val(data.consecutivo_novedad);
     $('#mantenimientoModal').modal('show');
  });
  $(tbody).on("click","button.btnver",function(){
    var data=table.row($(this).parents("tr")).data();
    location.assign('detalle/'+data.id);
   
     
      
   
  });
   
}


  

   

 

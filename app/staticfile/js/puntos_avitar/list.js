
$('input[name="search"]').autocomplete({
    source: function (request, response) {
      $.ajax({
        url: window.location.pathname,
        type: "POST",
        data: {
          action: "autocomplete",
          term: request.term,
        },
        dataType: "json",
      })
        .done(function (data) {
          response(data);
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
          alert(textStatus + ": " + errorThrown);
        })
        .always(function (data) {});
    },
    delay: 100,
    minLength: 3,
    select: function (event, ui) {
      
     
      $('input[name="ced"]').val(ui.item.cedula);  
      
      $("#data").DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
          url: window.location.pathname,
          type: "POST",
          data: {
            action: "searchdata",
            id:   ui.item.cedula,
          },
          dataSrc: "",
        },
        columns: [
          { data: "numero_factura" },
          { data: "total_factura" },
          { data: "vendedor" },
          { data: "fecha" },
          { data: "puntaje_total" },
          { data: "canjeados" },                
        ],
        columnDefs: [
          {
            targets: 3,
            render: DataTable.render.datetime('DD/MM/YYYY'),

                       
          },

          
        ],
        
        initComplete: function (settings, json) {
         var suma = $('#data').DataTable().column(4).data().sum();
         var suma1 = $('#data').DataTable().column(5).data().sum();
         var resta=suma-suma1;
          $('#total').html(suma);
          $('#total1').html(suma1);
          $('#resta').html(resta);
        },
     
      });
      
    },

   
});



    


 

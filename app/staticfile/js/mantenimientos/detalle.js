$(function(){
  
  var table=$("#data").DataTable({
    responsive: true,
    autoWidth: false,
    destroy: true,
    deferRender: true,
    order: [[2, 'asc']],
    order: [[3, 'asc']],
    ajax: {
      url: window.location.pathname,
      type: "POST",
      data: {
       
        
      },
      dataSrc: "",
    },

    columns: [
      { data: "usuario" },
      { data: "fecha" },
      { data: "hora" },
      { data: "contratista" },
      { data: "etapa" },
      { data: "estado" },
      { data: "id_mantenimiento" },
      { data: "usuario_reporte" },
      { data: "recibido_por" },  
      { data: "procedimientos" },
       
    ],
    columnDefs: [
      {
        targets: 1,
           
            //render: DataTable.render.datetime('DD/MM/YYYY'),
            visible: false,
            
                   
      },
      {
        targets: 2,
           
           // render: DataTable.render.datetime('DD/MM/YYYY'),
           // render: DataTable.render.datetime('THH:MM:SS'),
            visible: false,
                   
      },
      {
      targets: 6,
           
            visible: false,
            

                   
      },
    
        
        
    ],
    
    initComplete: function (settings, json) {
      
    },
  
  });
  

  optener_novedad(table) 


});
    
optener_novedad=function(table){
  $('#imprimir').on("click",function(){
    var data=table.row().data();
    location.href='reportePDF/'+data.id_mantenimiento;
  });
}


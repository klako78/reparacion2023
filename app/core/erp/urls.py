from django.urls import path
from django.contrib.auth.views import  LogoutView
from core.erp.views.puntos_avitar.views import *
from core.erp.views.inicio_avitar.views import *
from core.erp.views.inmuebles.views import *
from core.erp.views.pqrs.views import *
from core.erp.views.mantenimientos.views import *
from core.erp.views.reportes.views import *
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import static

app_name = 'erp'
urlpatterns = [
    #salir
    path('salir',LogoutView.as_view(next_page='login'),name='salir'),
    # home
    path('inicio',inicioCreateView.as_view(), name='inicio'),
    #puntaje
    path('list/<int:id>/<str:nom>',PuntajeListView.as_view(), name='list'),
    path('ventas',VentasListView.as_view(), name='ventas'),
    path('list_general',GeneralListView.as_view(), name='list_general'),
    path('list_general/delete/<int:pk>/', SaleDeleteView.as_view(), name='sale_delete'),
    #inmuebles
    path('inmueble',InmuebleCreateView.as_view(), name='inmueble'),
    path('inmuebleedit/<int:pk>/',InmuebleUpdateView.as_view(), name='inmuebleedit'),
    path('contratoedit/<int:pk>/',AdministracionUpdateView.as_view(), name='inmuebleedit'),
    path('fichamodal',fichamodalListView.as_view(), name='fichamodal'),
    path('tipos',tipo_clienteCreateView.as_view(), name='tipos'),
    path('ubicacion',ubicacionCreateView.as_view(), name='ubicacion'),
    path('clientes',clientesCreateView.as_view(), name='clientes'),
    path('tipoinmueble',tipoinmuebleCreateView.as_view(), name='tipoinmueble'),
    path('servicios',serviciosCreateView.as_view(), name='servicios'),
    path('administracionlist',AdministracionListView.as_view(), name='administracionlist'),
    path('corretajelist',CorretajeListView.as_view(), name='corretajelist'),
    path('arrendamientolist',ArrendamientoListView.as_view(), name='arrendamientolist'),
    #PRS
    path('pqrslist',pqrsListView.as_view(), name='pqrslist'),
    
    #mantenimientos
    path('novedadeslist',novedadListView.as_view(), name='novedadeslist'),
    path('mantenimientoslist',mantenimientosListView.as_view(), name='mantenimientoslist'),
    path('individual/<int:id>/<str:action>',mantenimientosListView1.as_view(), name='individual'),
    path('detalle/<int:id>',detallesListView.as_view(),name='detalle'),
    path('preventivos',fichaListView.as_view(),name='preventivos'),
    path('procedimiento',procedimientoCreateView.as_view(), name='procedimiento'),
   #reportes
    path('list/reporteclientePDF/<int:id>',InformeclientesPDFView.as_view(),name='reporteclientePDF'),
    path('detalle/reportePDF/<int:id>' ,InformePDFView.as_view(),name='reportePDF'),
    path('ventas/reporteventasPDF/<str:init>/<str:end>' ,InformeventasPDFView.as_view(),name='reporteventasPDF'),
    path('reportenovedadPDF/<str:estado>/<str:init>/<str:end>' ,InformenovedadPDFView.as_view(),name='reportenovedadPDF'),
    path('reportemantenimientoPDF/<str:estado>/<str:init>/<str:end>' ,InformemantenimientoPDFView.as_view(),name='reportemantenimientoPDF'),
    path('fichaPDF/<int:id>' ,FichaTecnicaPDFView.as_view(),name='fichaPDF'),
    path('adminPDF/<int:id>' ,ContratoAdminPDFView.as_view(),name='adminPDF'),
    path('corretajePDF/<int:id>' ,ContratoCorretajePDFView.as_view(),name='corretajePDF'),
    path('arrendamientoPDF/<int:id>' ,ContratoArendamientoPDFView.as_view(),name='arrendamientoPDF'),
    path('constructorPDF/<int:id>' ,ConstructorPDFView.as_view(),name='constructorPDF'),
    path('reporteinmueblesPDF/<str:estatus>/<str:contrato>' ,InformeinmueblesPDFView.as_view(),name='reporteinmueblesPDF'),
    
    #path('tipos',tipo_clienteCreateView.as_view(), name='tipos'),
      
   
   
   
   
    #path('fichalist',fichaListView.as_view(),name='fichalist'),
    
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


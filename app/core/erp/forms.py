from dataclasses import field, fields
from logging import PlaceHolder
from typing import Sequence, Tuple
from django.contrib.admin.sites import AdminSite
from django.contrib.admin.widgets import AutocompleteSelect
from django.contrib import admin
from django.forms import ModelForm
from django.db.models.fields import TextField
from django.forms import widgets
from django.forms.fields import ChoiceField
from django.utils.regex_helper import Choice
from .models import *
from django import forms

class ClientesForm(ModelForm):
    class Meta:
        model=clientes
        fields='__all__'
        exclude=['codigo','referencia','consecutivo','numero_mensual','numero_anual']
        widgets = {
            'fecha': forms.DateInput(
             format=('%Y-%m-%d'),
             attrs={'class': 'form-control', 
               'placeholder': 'Select a date',
               'type': 'date'
              }),
            }
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'})    
     
class PuntajesForm(ModelForm):
    class Meta:
        model=puntaje
        fields='__all__'
        exclude=['vendedor','puntaje_total','fecha_canje','canjeados','detalle_canje','operacion','nivel']
        widgets = {
            'fecha': forms.DateInput(
             format=('%Y-%m-%d'),
             attrs={'class': 'form-control', 
               'placeholder': 'Select a date',
               'type': 'date'
              }),
            }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'})   

class CanjesForm(ModelForm):
    class Meta:
        model=puntaje
        fields='__all__'
        exclude=['vendedor','puntaje_total','numero_factura','total_factura','operacion','nivel']
        widgets = {
            'fecha': forms.DateInput(
             format=('%Y-%m-%d'),
             attrs={'class': 'form-control', 
               'placeholder': 'Select a date',
               'type': 'date'
              }),
            }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'})   

#puntos avitar individual

class PuntajeindividualForm(ModelForm):
    class Meta:
        model=puntaje
        fields='__all__'
        exclude=['vendedor','puntaje_total','fecha_canje','canjeados','detalle_canje','operacion','cliente','nivel']
        widgets = {
            'fecha': forms.DateInput(
             format=('%Y-%m-%d'),
             attrs={'class': 'form-control', 
               'placeholder': 'Select a date',
               'type': 'date'
              }),
            }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'})   

class CanjeindividualForm(ModelForm):
    class Meta:
        model=puntaje
        fields='__all__'
        exclude=['vendedor','puntaje_total','numero_factura','total_factura','operacion','cliente','nivel']
        widgets = {
            'fecha': forms.DateInput(
             format=('%Y-%m-%d'),
             attrs={'class': 'form-control', 
               'placeholder': 'Select a date',
               'type': 'date'
              }),
            }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'})


class inventarioForm(ModelForm):
    class Meta:
        model=inmuebles
        fields='__all__'
        exclude=['detalle_mantenimiento','fecha','fecha_ultimo','fecha_mantenimiento']
        
              
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'})   
            self.fields[str(field)].widget.attrs.update(PlaceHolder=f'{str(field)}')
        
class Clientes_inmoviliariaForm(ModelForm):
    class Meta:
        model=clientes
        fields='__all__'
        exclude=['codigo','referencia','consecutivo','numero_mensual','numero_anual','fecha']
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'})                

class novedadForm(ModelForm):
    class Meta:
        model=novedades
        fields='__all__'
        exclude=['usuario','estado_novedad']
        widgets = {
            'fecha_novedad': forms.DateInput(
             format=('%Y-%m-%d'),
             attrs={'class': 'form-control', 
               'placeholder': 'Select a date',
               'type': 'date'
              }),
            }
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'})        

class tipo_clienteForm(ModelForm):
    class Meta:
        model=tipo_clentes
        fields='__all__'
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'})    


class tipoinmuebleForm(ModelForm):
    class Meta:
        model=tipos_inmuebles
        fields='__all__'
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'}) 

class procedimientosForm(ModelForm):
    class Meta:
        model=Procedimiento
        fields='__all__'
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'}) 


class tipo_negocioForm(ModelForm):
    class Meta:
        model=tipos_negocios
        fields='__all__'
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'}) 

class ubicacionForm(ModelForm):
    class Meta:
        model=ubicacion
        fields='__all__'
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'}) 

class mantenimientoForm(ModelForm):
    class Meta:
        model=mantenimientos
        fields='__all__'
        exclude=['consecutivo_novedad','fecha_inicio','etapa','usuario','inmueble','fecha_terminacion','estado_mantenimiento','usuario_novedad','descripcion_actividad','tipo_mantenimiento']
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'})      


class mantenimientoForm1(ModelForm):
    class Meta:
        model=mantenimientos
        fields='__all__'
        exclude=['consecutivo_novedad','fecha_inicio','usuario','inmueble','usuario_novedad']
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'})  

class fichaForm(ModelForm):
    class Meta:
        model=inmuebles
        fields=['fecha_ultimo','fecha_mantenimiento','detalle_mantenimiento']
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'})

class VisitaForm(ModelForm):
    class Meta:
        model=detalles_mantenimiento
        fields='__all__'
        exclude=['fecha_ultimo','fecha_mantenimiento','preventivo','responsable','usuario','etapa','inmueble','id_mantenimiento','contratista','usuario_reporte','recibido_por','fecha_etapa','procedimientos','autoricacion','finalizacion']
        widgets = {
            'fecha': forms.DateInput(
             format=('%Y-%m-%d'),
             attrs={'class': 'form-control', 
               'placeholder': 'Select a date',
               'type': 'date'
              }),
            }
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'})          

class PresupuestoForm(ModelForm):
    class Meta:
        model=detalles_mantenimiento
        fields='__all__'
        exclude=['fecha_ultimo','fecha_mantenimiento','preventivo','responsable','usuario','etapa','inmueble','contratista','id_mantenimiento','usuario_reporte','recibido_por','fecha_etapa','procedimientos','finalizacion']
        widgets = {
            'fecha': forms.DateInput(
             format=('%Y-%m-%d'),
             attrs={'class': 'form-control', 
               'placeholder': 'Select a date',
               'type': 'date'
              }),
            }
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'})   



class ejecucionForm(ModelForm):
    class Meta:
        model=detalles_mantenimiento
        fields='__all__'
        exclude=['usuario','etapa','inmueble','id_mantenimiento','usuario_reporte','recibido_por','fecha_etapa','autoricacion']
        widgets = {
            'fecha_mantenimiento': forms.DateInput(
             format=('%Y-%m-%d'),
             attrs={'class': 'form-control', 
               'placeholder': 'Select a date',
               'type': 'date'
              }),
            'fecha_ultimo': forms.DateInput(
             format=('%Y-%m-%d'),
             attrs={'class': 'form-control', 
               'placeholder': 'Select a date',
               'type': 'date'
              }),
            'fecha': forms.DateInput(
             format=('%Y-%m-%d'),
             attrs={'class': 'form-control', 
               'placeholder': 'Select a date',
               'type': 'date'
              }),
}
            
            
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'})   

class EntregaForm(ModelForm):
    class Meta:
        model=detalles_mantenimiento
        fields='__all__'
        exclude=['fecha_ultimo','fecha_mantenimiento','preventivo','responsable','usuario','contratista','etapa','procedimientos','finalizacion','inmueble','id_mantenimiento','usuario_reporte','fecha_etapa','autoricacion']
        widgets = {
            'fecha': forms.DateInput(
             format=('%Y-%m-%d'),
             attrs={'class': 'form-control', 
               'placeholder': 'Select a date',
               'type': 'date'
              }),}
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'})   

class administracionForm(ModelForm):
    class Meta:
        model=contratos
        fields='__all__'
        exclude=['arrendatario','precio','categorias','codigo','referencia','consecutivo','numero_mensual','numero_anual','tipo_inmueble','tipo_contrato','representante_legal','codeudor']
        widgets = {
            'fecha': forms.DateInput(
             format=('%Y-%m-%d'),
             attrs={'class': 'form-control', 
               'placeholder': 'Select a date',
               'type': 'date'
              }),
            }
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'})          


class arrendamientoForm(ModelForm):
    class Meta:
        model=contratos
        fields='__all__'
        exclude=['codigo','referencia','consecutivo','numero_mensual','numero_anual','categorias','tipo_contrato','representante_legal','linderos','precio']
        widgets = {
            'fecha': forms.DateInput(
             format=('%Y-%m-%d'),
             attrs={'class': 'form-control', 
               'placeholder': 'Select a date',
               'type': 'date'
              }),
            }
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'})   

class RepresentanteForm(ModelForm):
    class Meta:
        model=representantes
        fields='__all__'
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'})                


class codeudorForm(ModelForm):
    class Meta:
        model=codeudores
        fields='__all__'
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'})  

class editForm(ModelForm):
    class Meta:
        model=inmuebles
        fields=('observacion','linderos','precio_alquiler','precio_venta','foto','foto1','foto2','foto3','ubicacion')
        # exclude=['detalle_mantenimiento','fecha','fecha_ultimo','fecha_mantenimiento']
        
              
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'})   
            self.fields[str(field)].widget.attrs.update(PlaceHolder=f'{str(field)}')              


class editcontratoForm(ModelForm):
    class Meta:
        model=cuerpoContrato
        fields='__all__'
        exclude=['tipo_contrato']
        
              
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'})  
            
            self.fields['cuerpo'].label = "" 
                          
class compañiasForm(ModelForm):
    class Meta:
        model=compañias
        fields='__all__'
            
              
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'})  
            
class CuentasForm(ModelForm):
    class Meta:
        model=cuentas
        fields='__all__'
            
              
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            # Recorremos todos los campos del modelo para añadirle class="form-control
            self.fields[field].widget.attrs.update({'class': 'form-control'})             
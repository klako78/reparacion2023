from rest_framework import serializers
from .models import clientes,inmuebles,tipos_inmuebles,ubicacion,detalles_mantenimiento,Procedimiento,puntaje,contratos

class ClientesSerializer(serializers.ModelSerializer):
    class Meta:
        model = clientes
        fields = "__all__"
        read_only_fields = (
            
        )


class Tipo_inmuebleSerializer(serializers.ModelSerializer):
    class Meta:
        model = tipos_inmuebles
        fields = "__all__"
        read_only_fields = (
            
        )

class UbicacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = ubicacion
        fields = "__all__"
        read_only_fields = (
            
        )

class ProcedimientosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Procedimiento
        fields = "__all__"
        read_only_fields = (
            
        )

class DetalleSerializer(serializers.ModelSerializer):
    procedimientos=serializers.StringRelatedField()
    class Meta:
        model = detalles_mantenimiento
        fields = "__all__"
        read_only_fields = (
            
        )



class InmueblesSerializer(serializers.ModelSerializer):
    clientes=serializers.StringRelatedField()
    categorias=serializers.StringRelatedField()
    ubucacion=serializers.StringRelatedField()
    class Meta:
        model = inmuebles
        fields = "__all__"
        read_only_fields = (
           
        )

class puntajeSerializer(serializers.ModelSerializer):
    cliente=serializers.StringRelatedField()
      

    class Meta:
        model = puntaje
        fields = "__all__"
        read_only_fields = (
           
        )
        
class contratosSerializer(serializers.ModelSerializer):
    inmueble=serializers.StringRelatedField()
    arrendatario=serializers.StringRelatedField()
    codeudor=serializers.StringRelatedField()  
    categorias=serializers.StringRelatedField()  
    class Meta:
        model = contratos
        fields = "__all__"
        read_only_fields = (
           
        )    

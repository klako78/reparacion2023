from functools import total_ordering
from http import client
from multiprocessing import context
from reprlib import recursive_repr
from turtle import position
from django.contrib.auth import views
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.shortcuts import redirect, render 
from django.http import HttpResponse, JsonResponse,HttpResponseRedirect
from django.views.generic import *
from core.erp.models import *
from django.db.models import Q
from django.contrib.auth.views import LoginView
from django.contrib.auth.decorators import login_required
from core.erp.forms import *
import os
from django.conf import settings
from django.template.loader import get_template
from django.contrib.staticfiles import finders
from django.contrib import messages
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import login, logout
from django.contrib.auth.forms import AuthenticationForm
from core.erp.serializers import InmueblesSerializer,DetalleSerializer,Tipo_inmuebleSerializer
from rest_framework.response import Response 
from rest_framework.views import APIView
from xhtml2pdf import pisa
from django.views.generic import CreateView, ListView, DeleteView, UpdateView
from datetime import date
from datetime import datetime
from core.erp.mixins import ValidatePermissionRequiredMixin
# Create your views here.
class novedadListView(LoginRequiredMixin,ValidatePermissionRequiredMixin,ListView):
    permission_required=('Logistica y Compras')
    model = novedades
    template_name = 'mantenimientos/list.html'
    login_url='login'
    redirect_field_name='next'
    success_url="erperp:mantenimientoslist"
    @method_decorator(csrf_exempt) 
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def post(self, request, *args, **kwargs):
        data = {}
        
        try:
            action = request.POST['action']
            
            if action == 'searchdata':
                data = []
                for i in novedades.objects.filter(estado_novedad='RADICADA'):
                    data.append(i.toJSON())
            elif action=='add':
                nov=novedades.objects.get(id=request.POST['novedad'])
                nov.estado_novedad='EN EJECUCION'
                fecha=nov.fecha
                nov.save()
                cli=mantenimientos()
                cli.descripcion_actividad=request.POST['descripcion_actividad']
                cli.usuario=request.user.first_name +" "+ request.user.last_name
                cli.usuario_novedad=request.POST['usuario_novedad']
                cli.estado_mantenimiento='EN EJECUCION'
                cli.inmueble=request.POST['cedula']
                cli.consecutivo_novedad=request.POST['novedad']
                cli.etapa='Novedad'
                cli.save()
                novedad=mantenimientos.objects.latest()
                detalle=detalles_mantenimiento()
                detalle.descripcion_actividad=novedad.descripcion_actividad
                detalle.id_mantenimiento=novedad.id
                detalle.usuario=request.user.first_name +" "+ request.user.last_name
                detalle.usuario_reporte=novedad.usuario_novedad
                detalle.inmueble=novedad.inmueble
                detalle.etapa='Novedad'
                detalle.estado='EJECUTADO'
                detalle.fecha_etapa=fecha
                detalle.fecha=fecha
                detalle.save()

            elif action == 'autocomplete':
                data = []
                for i in inmuebles.objects.filter(direccion__icontains=request.POST['term'] )[0:10]:
                    item = i.toJSON()
                    item['value'] = i.direccion
                    data.append(item)        
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Novedades'
        context['create_url'] = reverse_lazy('erp:novedadeslist')
        context['list_url'] = reverse_lazy('erp:novedadeslist')
        context['entity'] = 'mantenimientos'
        context['form'] = mantenimientoForm
        return context           
    
class mantenimientosListView(LoginRequiredMixin,ValidatePermissionRequiredMixin,ListView):
    permission_required=('Logistica y Compras')
    model = mantenimientos
    form_class=ejecucionForm
    template_name = 'mantenimientos/listmantenimientos.html'
    login_url='login'
    redirect_field_name='next'
    success_url=reverse_lazy="erp:mantenimientoslist"
    @method_decorator(csrf_exempt) 
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
       
        data = {}
       

        try:
            action = request.POST['action']
            if action== 'searchall':
                data = []
                for i in mantenimientos.objects.all():
                    data.append(i.toJSON())
            elif action == 'fecha':
                inicio= request.POST['inicio']
                fin=request.POST['fin']
                estado=request.POST['estado']
                data = []
                if inicio and estado!="no":
                    for i in mantenimientos.objects.filter(fecha_inicio__range=[inicio,fin],etapa=estado):
                        data.append(i.toJSON())
                elif len(inicio) == 0 and estado!="no":
                    for i in mantenimientos.objects.filter(etapa=estado):
                        data.append(i.toJSON())
                elif estado=="no" and inicio:
                    for i in mantenimientos.objects.filter(fecha_inicio__range=[inicio,fin]):
                        data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
            pass
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        
        context = super().get_context_data(**kwargs)
        context['title'] = 'Mantenimiento'
        context['create_url'] = reverse_lazy('erp:mantenimientoslist')
        context['list_url'] = reverse_lazy('erp:mantenimientoslist')
        context['entity'] = 'mantenimientos'
        return context           

class mantenimientosListView1(LoginRequiredMixin,ValidatePermissionRequiredMixin,ListView):
    permission_required=('Logistica y Compras')
    model = mantenimientos
    form_class=ejecucionForm
    template_name = 'mantenimientos/listmantenimientosindividual.html'
    login_url='login'
    redirect_field_name='next'
    success_url=reverse_lazy="erp:mantenimientoslist"
    @method_decorator(csrf_exempt) 
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def post(self, request, *args, **kwargs):
        action = request.POST['action']
        cont = self.kwargs.get('id', None)
         
        data = {}
        
        try:
           
            if action == 'searchdata':
                data = []
                for i in mantenimientos.objects.filter(consecutivo_novedad=cont):
                    data.append(i.toJSON())
            elif action=='add':
                id=request.POST['mantenimiento']
                det=mantenimientos.objects.get(id=id) 
                detalles=detalles_mantenimiento.objects.filter(id_mantenimiento=id).latest()
                etapa=det.etapa
                final=detalles.finalizacion
                auto=detalles.autoricacion
                if etapa== "Novedad":
                    stage="Inspección"
                elif etapa=="Inspección":
                    stage="Presupuesto"
                elif etapa=="Presupuesto" and auto=="SI":
                    stage="Ejecucion"
                elif etapa=="Ejecucion" and final=="NO":
                    stage="Ejecucion" 
                else:
                    stage=etapa
                estado=request.POST['estado']
                man=mantenimientos.objects.get(id=id)
                man.descripcion_actividad=request.POST['descripcion_actividad']
                man.etapa=stage
                if stage=='Entrega':
                    if estado=='EJECUTADO':
                        man.estado_mantenimiento='EJECUTADO'
                    else:
                        man.estado_mantenimiento='NO EJECUTADO'    
                    man.fecha_terminacion= request.POST["fecha"]
                man.save()
                nov=novedades.objects.get(id=request.POST['id_novedad'])
                nov.novedad=request.POST['descripcion_actividad']
                nov.fecha=request.POST["fecha"]
                if stage=='Entrega':
                    if estado=='EJECUTADO':
                        nov.estado_novedad='EJECUTADO'
                    else:
                        nov.estado_novedad='NO EJECUTADO'
                nov.save()
                if stage=="Inspección":
                    form=detalles_mantenimiento()
                    form.etapa="Inspección"
                    form.descripcion_actividad=request.POST["descripcion_actividad"]
                    form.inmueble=request.POST["id_inmueble"]
                    form.id_mantenimiento=request.POST["mantenimiento"]
                    form.usuario=request.user.first_name +" "+ request.user.last_name
                    form.estado=request.POST["estado"]
                    form.fecha=request.POST["fecha"]
                    form.save()
                elif stage=="Presupuesto":
                    form=detalles_mantenimiento()
                    form.etapa="Presupuesto"
                    form.descripcion_actividad=request.POST["descripcion_actividad"]
                    form.inmueble=request.POST["id_inmueble"]
                    form.id_mantenimiento=request.POST["mantenimiento"]
                    form.usuario=request.user.first_name +" "+ request.user.last_name
                    form.estado=request.POST["estado"]
                    form.autoricacion=request.POST["autoricacion"]
                    form.fecha=request.POST["fecha"]
                    form.save()
                elif stage=="Ejecucion":
                    form1=self.form_class(request.POST)
                    if form1.is_valid():
                        form1.save()
                    det=detalles_mantenimiento.objects.latest()
                    det.etapa="Ejecucion"
                    det.inmueble=request.POST["id_inmueble"]
                    det.id_mantenimiento=request.POST["mantenimiento"]
                    det.usuario=request.user.first_name +" "+ request.user.last_name
                    det.fecha=request.POST["fecha"]
                    if request.POST["preventivo"]=="SI":
                        det.preventivo=request.POST["preventivo"]
                        det.responsable=request.POST["responsable"]
                        det.fecha_ultimo=request.POST["fecha_ultimo"]
                        det.fecha_mantenimiento=request.POST["fecha_mantenimiento"]
                    det.save()
                    entr=detalles_mantenimiento.objects.latest()
                    cod=entr.id_mantenimiento
                    final=entr.finalizacion
                    if final=="SI":
                        formentr=mantenimientos.objects.get(id=cod)
                        formentr.etapa="Entrega"
                        formentr.save()
                       
                else:
                    form=detalles_mantenimiento()
                    form.etapa="Entrega"
                    form.descripcion_actividad=request.POST["descripcion_actividad"]
                    form.inmueble=request.POST["id_inmueble"]
                    form.id_mantenimiento=request.POST["mantenimiento"]
                    form.usuario=request.user.first_name +" "+ request.user.last_name
                    form.estado=request.POST["estado"]
                    form.recibido_por=request.POST["recibido_por"]
                    form.fecha=request.POST["fecha"]
                    form.save()

            elif action== 'add1':
                proc=Procedimiento()
                proc.procedimientos=request.POST['procedimientos']
                proc.save()
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
            pass
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        cont = self.kwargs.get('id', None)
        det=mantenimientos.objects.get(consecutivo_novedad=cont)
        mantenimientoID=det.id
        etapa=det.etapa
        detalle=detalles_mantenimiento.objects.filter(id_mantenimiento=mantenimientoID).latest()
        context = super().get_context_data(**kwargs)
        context['title'] = 'Mantenimiento'
        context['create_url'] = reverse_lazy('erp:mantenimientoslist')
        context['list_url'] = reverse_lazy('erp:mantenimientoslist')
        context['entity'] = 'mantenimientos'
        context['formp'] = procedimientosForm
        if etapa=="Novedad":
            context['title'] = 'Inspección'
            context['formd'] = VisitaForm
        elif etapa=="Inspección":
            context['title'] = 'Presupuesto'
            context['formd'] = PresupuestoForm
        elif etapa=="Presupuesto" and detalle.autoricacion=="SI":
            context['title'] = 'Ejecucion'
            context['formd'] = ejecucionForm
        elif etapa=="Ejecucion" and detalle.finalizacion=="NO":
             context['title'] = 'Ejecucion'
             context['formd'] = ejecucionForm
        elif etapa=="Entrega" and not detalle.recibido_por:
            context['title'] = 'Entrega'
            context['formd'] = EntregaForm
        return context           

class detallesListView(LoginRequiredMixin,ValidatePermissionRequiredMixin,ListView):
    permission_required=('Logistica y Compras')
    model = detalles_mantenimiento
    form_class=inventarioForm
    template_name = 'mantenimientos/listdetalle.html'
    login_url='login'
    redirect_field_name='next'
    
    @method_decorator(csrf_exempt) 
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def post(self, request, *args, **kwargs):
        id= self.kwargs.get('id', None) 
        form=self.form_class(request.POST)
        if form.is_valid:
            form.save()
        else:
                print(form.subject.errors)
        #try:
        detalles=detalles_mantenimiento.objects.filter(id_mantenimiento=id).order_by('id')
        detalleserializer=DetalleSerializer(detalles,many=True)
       
        #except Exception as e:
            #data['error'] = str(e)
       
        return JsonResponse(detalleserializer.data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Detalles'
        context['create_url'] = reverse_lazy('detalle')
        context['list_url'] = reverse_lazy('detalle')
        context['entity'] = 'detalles'
        context['form'] = inventarioForm
        return context 

class fichaListView(LoginRequiredMixin,ValidatePermissionRequiredMixin,ListView):
    permission_required=('Logistica y Compras')
    model = inmuebles
    template_name = 'mantenimientos/preventivo.html'
    login_url='login'
    redirect_field_name='next'
    success_url=reverse_lazy("erp:fichalist")
    @method_decorator(csrf_exempt) 
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def post(self, request, *args, **kwargs):
        try:
            inmueble=inmuebles.objects.all()
            inmueble_serializer=InmueblesSerializer(inmueble,many=True)
           
            action = request.POST['action']
            if action=='add':
                inm=inmuebles.objects.get(id=request.POST['id'])
                inm.fecha_ultimo=request.POST['fecha_ultimo']
                inm.fecha_mantenimiento=request.POST['fecha_mantenimiento']
                inm.detalle_mantenimiento=request.POST['detalle_mantenimiento']
                inm.save()
            elif action=='add2':
                formin=self.form_class(request.POST)
                if formin.is_valid():
                    formin.save()

        except Exception as e:
            inmueble_serializer['error'] = str(e)
        return JsonResponse(inmueble_serializer.data,safe=False)
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Mantenimiento preventivo'
        context['create_url'] = reverse_lazy('erp:fichalist')
        context['list_url'] = reverse_lazy('erp:fichalist')
        context['entity'] = 'inmueble'
        context['form'] = fichaForm
        context['formin'] = inventarioForm
        return context           

class procedimientoCreateView(LoginRequiredMixin,ValidatePermissionRequiredMixin,CreateView):
    permission_required=('Logistica y Compras')
    model=Procedimiento
    form_class=procedimientosForm
    template_name='mantenimientos/Formulariotipo.html'
    login_url='login'
    redirect_field_name='next' 
    def get_context_data(self, **kwargs):
        context=super().get_context_data(**kwargs)
        context['title']='Crear Procedimiento'
        return context        


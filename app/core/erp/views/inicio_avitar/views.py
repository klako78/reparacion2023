from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt




class inicioCreateView(LoginRequiredMixin,TemplateView):
    template_name = 'inicio/inicio.html'
    login_url='login'
    redirect_field_name='next'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['panel'] = 'Panel de administrador'
        context['title']='Home'
        return context

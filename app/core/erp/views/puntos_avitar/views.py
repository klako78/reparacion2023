from django.contrib.auth.mixins import LoginRequiredMixin,PermissionRequiredMixin
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView,DeleteView
from core.erp.serializers import puntajeSerializer,ClientesSerializer
from core.erp.forms import ClientesForm,PuntajesForm,CanjesForm,PuntajeindividualForm,CanjeindividualForm,compañiasForm,CuentasForm
from core.erp.mixins import ValidatePermissionRequiredMixin
from core.erp.models import puntaje,clientes,compañias
from django.db.models.aggregates import Sum
import json
from django.core.serializers.json import DjangoJSONEncoder
from django.core.serializers import serialize
from django.core import serializers
from decimal import Decimal
from django.db.models import F
import qrcode
from PIL import Image
from core.erp.generar_codigo import *
from datetime import date, timedelta
class PuntajeListView(LoginRequiredMixin,ValidatePermissionRequiredMixin,ListView):
    permission_required = ('Ferreteria','Administrador')
    model = puntaje
    form_class=PuntajesForm
    form_classCanj=CanjesForm
    template_name = 'puntos_avitar/list.html'
    
    login_url='login'
    redirect_field_name='next'
    @method_decorator(csrf_exempt) 
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def post(self, request, *args, **kwargs):
        id = self.kwargs.get('id', None) 
        
        data = {}
        
        try:
            action = request.POST['action']
            
            if action == 'searchdata':
                data = []
                
                for i in puntaje.objects.filter(cliente=id,operacion="PUNTOS"):
                    data.append(i.toJSON())
                    
            elif action=='searchdata1':
                data = []
                for i in puntaje.objects.filter(cliente=id,operacion="CANJE"):
                    data.append(i.toJSON())
            elif action=='add':
                cli=clientes()
                cli.cedula=request.POST['cedula']
                cli.nombre=request.POST['nombre']
                cli.correo_electronico=request.POST['correo_electronico']
                cli.telefono=request.POST['telefono']
                cli.tipo_cliente_id=request.POST['tipo_cliente']
                cli.tipo_persona=request.POST['tipo_persona']
                cli.ciudad_domicilo=request.POST['ciudad_domicilio']
                cli.ciudad_expedicion=request.POST['ciudad_expedicion']
                cli.direccion=request.POST['direccion']
                cli.save()
                qr=qrcode.QRCode(
                    version=1,
                    error_correction=qrcode.constants.ERROR_CORRECT_L,
                    box_size=10,
                    border=4,
                )
                qr.add_data('Some data')
                qr.make(fit=True)
                img = qr.make_image(fill_color="black", back_color="white")

            elif action=='add1':
                cli=puntaje()
                cli.numero_factura=request.POST['numero_factura']
                cli.vendedor=request.user.first_name +" "+ request.user.last_name
                cli.total_factura=int(request.POST['total_factura'])
                cli.puntaje_total=int(request.POST['total_factura'])/1000
                cli.fecha=request.POST['fecha']
                cli.operacion="PUNTOS"
                cli.cliente_id=int(request.POST['cedula'])
                cli.save()
            elif action=='add2':
                cli=puntaje()
                cli.detalle_canje=request.POST['detalle_canje']
                cli.vendedor=request.user.first_name +" "+ request.user.last_name
                cli.canjeados=int(request.POST['canjeados'])
                cli.fecha=request.POST['fecha']
                cli.operacion="CANJE"
                cli.cliente_id=int(request.POST['cedula'])
                cli.save()   
            elif action == 'autocomplete':
                data = []
                for i in clientes.objects.filter(nombre__icontains=request.POST['term'] )[0:10]:
                    item = i.toJSON()
                    item['value'] = i.nombre
                    data.append(item)        
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        nombre = self.kwargs.get('nom', None)
        cedula = self.kwargs.get('id', None)
        fin=datetime.now() 
        inicio = fin - timedelta(days=60)
        rang_fecha=puntaje.objects.filter(fecha__range=[inicio,fin]).filter(cliente_id=int(cedula)).aggregate(totalf=Sum("total_factura"))
        suma=rang_fecha['totalf']
        if suma <= 7000000:
            nivel=1
        elif suma > 7000000 and suma <= 10000000:
            nivel=2
        else:
            nivel=3                
        context = super().get_context_data(**kwargs)
        context['title'] = 'Puntos Acumulados'
        context['name'] = nombre
        context['id'] = cedula
        context['nivel'] = nivel
        context['create_url'] = reverse_lazy('list')
        context['list_url'] = reverse_lazy('list')
        context['entity'] = 'puntaje'
        context['formp'] = PuntajeindividualForm
        context['formc'] = CanjeindividualForm
        return context    

class VentasListView(LoginRequiredMixin,ValidatePermissionRequiredMixin,ListView):
    permission_required = ('Ferreteria','Administrador')
    model = puntaje
    form_class=PuntajesForm
    form_classCanj=CanjesForm
    template_name = 'puntos_avitar/ventas.html'
    
    login_url='login'
    redirect_field_name='next'
    @method_decorator(csrf_exempt) 
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def post(self, request, *args, **kwargs):
        
        data={}
        action = request.POST['action']
        if action== 'searchall':
            data=[]
            for i in clientes.objects.values("nombre","cedula").annotate(totalf=Sum('puntaje__total_factura'),totalp=Sum('puntaje__puntaje_total'),totalc=Sum('puntaje__canjeados'),totald=(F("totalp")-F("totalc"))):
                if i['totalf']:
                    data.append(i)
        elif action=='add':
                cliente=clientes.objects.filter(tipo_cliente=int(request.POST['tipo_cliente'])).last()
                if cliente:
                    fecha=fecha = datetime.strptime(request.POST['fecha'], "%Y-%m-%d")
                    fecha1=cliente.fecha
                    if fecha.month==fecha1.month:
                        numeroconm=cliente.numero_mensual+1
                    elif fecha.month != fecha1.month:
                        numeroconm=1
                    if fecha.year==fecha1.year:
                        numerocona=cliente.numero_anual+1
                    elif fecha.year != fecha1.year:
                        numerocona=1
                else:
                    numerocona=1
                    numeroconm=1
                cli=clientes()
                cli.numero_mensual=numeroconm
                cli.numero_anual=numerocona
                cli.consecutivo=consecutivos(request.POST['fecha'],numeroconm,numerocona)
                cli.referencia=ref1(request.POST['tipo_cliente'])
                cli.cedula=request.POST['cedula']
                cli.nombre=request.POST['nombre']
                cli.correo_electronico=request.POST['correo_electronico']
                cli.telefono=request.POST['telefono']
                cli.tipo_cliente_id=request.POST['tipo_cliente']
                cli.tipo_persona=request.POST['tipo_persona']
                cli.ciudad_domicilo=request.POST['ciudad_domicilo']
                cli.ciudad_expedicion=request.POST['ciudad_expedicion']
                cli.direccion=request.POST['direccion']
                cli.genero=request.POST['genero']
                cli.save()
                name=request.POST['cedula']+".png"
                qr=qrcode.QRCode(
                    version=1,
                    error_correction=qrcode.constants.ERROR_CORRECT_L,
                    box_size=10,
                    border=4,
                )
                qr.clear()
                qr.add_data('http://167.71.22.135:8000/erp/list/'+request.POST['cedula']+'/'+request.POST['nombre'])
                qr.make(fit=True)
                img = qr.make_image(fill_color="black", back_color="white")
                img.save('/home/klako/avitar_recargado/app/media/'+name)
        elif action=='add1':
                form=self.form_class(request.POST)
                if form.is_valid():
                        form.save()
                cli=puntaje.objects.latest()
                cli.numero_factura=request.POST['numero_factura']
                cli.vendedor=request.user.first_name +" "+ request.user.last_name
                cli.total_factura=int(request.POST['total_factura'])
                cli.puntaje_total=int(request.POST['total_factura'])/1000
                cli.fecha=request.POST['fecha']
                cli.operacion="PUNTOS"
                cli.save()
        elif action=='add2':
                form=self.form_classCanj(request.POST)
                if form.is_valid():
                        form.save()
                cli=puntaje.objects.latest()
                cli.detalle_canje=request.POST['detalle_canje']
                cli.vendedor=request.user.first_name +" "+ request.user.last_name
                cli.canjeados=int(request.POST['canjeados'])
                cli.fecha=request.POST['fecha']
                cli.operacion="CANJE"
                cli.save()
        elif action=='add5':
                cli=compañias()
                cli.nit=request.POST['nit']
                cli.razon_social=request.POST['razon_social']
                cli.telefono=request.POST['telefono']
                cli.direccion=request.POST['direccion']
                cli.ciudad=request.POST['ciudad']
                cli.cliente_id=request.POST['cliente']
                cli.save()
        elif action=='add6':
                cli=cuentas()
                cli.nombre_banco=request.POST['nombre_banco']
                cli.tipo_cuenta=request.POST['tipo_cuenta']
                cli.numero_cuenta=request.POST['numero_cuenta']
                cli.cliente_id=request.POST['cliente']
                cli.save()                    
        elif action=="fecha":
            data=[]
            inicio=request.POST.get('init')
            fin=request.POST.get('end')
            for i in clientes.objects.values("nombre","cedula").filter(puntaje__fecha__range=[inicio, fin]).annotate(totalf=Sum('puntaje__total_factura'),totalp=Sum('puntaje__puntaje_total'),totalc=Sum('puntaje__canjeados'),totald=(F("totalp")-F("totalc"))):
                if i['totalf']:
                    data.append(i)
        return JsonResponse(data, safe=False)       

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Puntos Avitar'
        context['create_url'] = reverse_lazy('ventas')
        context['list_url'] = reverse_lazy('ventas')
        context['entity'] = 'puntaje'
        context['form'] = ClientesForm
        context['formp'] = PuntajesForm
        context['formc'] = CanjesForm
        context['formcom'] = compañiasForm
        context['formcuentas'] = CuentasForm
        return context    




class GeneralListView(LoginRequiredMixin,ValidatePermissionRequiredMixin,ListView):
    permission_required=('Ferreteria','Administrador')
    model = puntaje
    template_name = 'puntos_avitar/list_general.html'
    login_url='login'
    redirect_field_name='next'
    @method_decorator(csrf_exempt) 
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def post(self, request, *args, **kwargs):
        action = request.POST['action']
        if action== 'searchall':
            puntos=puntaje.objects.filter(operacion="PUNTOS")
            puntosserializer=puntajeSerializer(puntos,many=True)         
        elif action=="fecha":
            inicio=request.POST.get('init')
            fin=request.POST.get('end')
            puntos=puntaje.objects.filter(fecha__range=[inicio, fin])
            puntosserializer=puntajeSerializer(puntos,many=True)
        return JsonResponse(puntosserializer.data, safe=False)       

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado Ventas'
        context['create_url'] = reverse_lazy('ventas')
        context['list_url'] = reverse_lazy('ventas')
        context['entity'] = 'puntaje'
        return context    

class SaleDeleteView(LoginRequiredMixin,ValidatePermissionRequiredMixin,DeleteView):
    permission_required=('Ferreteria','Administrador')
    model = puntaje
    template_name = 'puntos_avitar/delete.html'
    success_url = reverse_lazy('erp:list_general')
    #permission_required = 'erp.delete_sale'
    url_redirect = success_url

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de una Factura'
        context['entity'] = 'puntaje'
        context['list_general'] = self.success_url
        return context
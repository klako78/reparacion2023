from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.http import JsonResponse
from core.erp.models import *
from core.erp.forms import *
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView
from core.erp.mixins import ValidatePermissionRequiredMixin
# Create your views here.

class pqrsListView(LoginRequiredMixin,ValidatePermissionRequiredMixin,ListView):
    permission_required=('Logistica y Compras')
    model = novedades
    template_name = 'pqrs/list.html'
    login_url='login'
    redirect_field_name='next'
    @method_decorator(csrf_exempt) 
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def post(self, request, *args, **kwargs):
        data = {}
        
        try:
            

            action = request.POST['action']
            if action== 'searchall':
                data = []
                for i in novedades.objects.all():
                    data.append(i.toJSON())
            elif action == 'fecha':
                inicio= request.POST['inicio']
                fin=request.POST['fin']
                estado=request.POST['estado']
                data = []
                if inicio and estado!="no":
                    for i in novedades.objects.filter(fecha_novedad__range=[inicio,fin],estado_novedad=estado):
                        data.append(i.toJSON())
                elif len(inicio) == 0 and estado!="no":
                    for i in novedades.objects.filter(estado_novedad=estado):
                        data.append(i.toJSON())
                elif estado == "no" and inicio:
                    for i in novedades.objects.filter(fecha_novedad__range=[inicio,fin]):
                        data.append(i.toJSON())
            elif action=='add':
                cli=novedades()
                cli.inmueble_novedad_id=request.POST['inmueble_novedad']
                cli.novedad=request.POST['novedad']
                cli.estado_novedad='RADICADA'
                cli.usuario=request.user.first_name +" "+ request.user.last_name
                cli.usuario_novedad=request.POST['usuario_novedad']
                cli.fecha_novedad=request.POST['fecha_novedad']
                cli.save()
            elif action=='mant':
                nov=novedades.objects.get(id=request.POST['novedad'])
                fecha=nov.fecha_novedad
                nov.estado_novedad='EN EJECUCION'
                nov.save()
                cli=mantenimientos()
                cli.descripcion_actividad=request.POST['descripcion_actividad']
                cli.usuario=request.user.first_name +" "+ request.user.last_name
                cli.usuario_novedad=request.POST['usuario_novedad']
                cli.estado_mantenimiento='EN EJECUCION'
                cli.inmueble=request.POST['cedula']
                cli.consecutivo_novedad=request.POST['novedad']
                cli.etapa='Novedad'
                cli.fecha_inicio=fecha
                cli.save()
                novedad=mantenimientos.objects.latest()
                detalle=detalles_mantenimiento()
                detalle.descripcion_actividad=novedad.descripcion_actividad
                detalle.id_mantenimiento=novedad.id
                detalle.usuario=request.user.first_name +" "+ request.user.last_name
                detalle.usuario_reporte=novedad.usuario_novedad
                detalle.inmueble=novedad.inmueble
                detalle.etapa='Novedad'
                detalle.estado='EJECUTADO'
                detalle.fecha_etapa=fecha
                detalle.fecha=fecha
                detalle.save()
              
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Mantenimientos'
        context['create_url'] = reverse_lazy('pqrslist')
        context['list_url'] = reverse_lazy('pqrslist')
        context['entity'] = 'novedades'
        context['form'] = novedadForm
        context['formM'] = mantenimientoForm
        
        return context  


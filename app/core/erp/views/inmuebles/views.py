from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import *
from core.erp.models import clientes,inmuebles,ubicacion,tipos_inmuebles,tipos_negocios,contratos,compañias
from core.erp.forms import *
from django.views.generic import CreateView,UpdateView
from django.urls import reverse_lazy
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from core.erp.serializers import InmueblesSerializer,contratosSerializer
from django.http import  JsonResponse
from core.erp.mixins import ValidatePermissionRequiredMixin
from core.erp.generar_codigo import *
class InmuebleCreateView(LoginRequiredMixin,ValidatePermissionRequiredMixin,CreateView):
    permission_required=('Logistica y Compras','Marketing y Ventas')
    model=inmuebles
    form_class=inventarioForm
    template_name='inmuebles/FormularioInmuebles.html'
    login_url='login'
    redirect_field_name='next'
    success_url = reverse_lazy('erp:inmueble')
    @method_decorator(csrf_exempt)
    def get_initial(self):
        usuario = self.request.user.get_full_name
        self.initial['usuario'] = usuario   #esto tienes que modificar para ajustar a tus modelos. Explicacion abajo
        return self.initial.copy()
    
         
       
    def get_context_data(self, **kwargs):
        context=super().get_context_data(**kwargs)
        context['formc']=ClientesForm
        context['formt']=tipoinmuebleForm
        context['formn']=tipo_negocioForm
        context['formu']=ubicacionForm
        context['title']='Crear Inmueble'
        context['create_url'] = reverse_lazy('inmueble')
        context['list_url'] = reverse_lazy('inmueble')
        context['entity'] = 'inmuebles'
        return context


class clientesCreateView(LoginRequiredMixin,ValidatePermissionRequiredMixin,CreateView):
    permission_required=('Logistica y Compras','Marketing y Ventas')
    model=clientes
    form_class=ClientesForm
    login_url='login'
    redirect_field_name='next'
    success_url = reverse_lazy('erp:inmueble')
    def get_context_data(self, **kwargs):
        context=super().get_context_data(**kwargs)
        context['title']='Crear Cliente'
        return context


class ubicacionCreateView(LoginRequiredMixin,ValidatePermissionRequiredMixin,CreateView):
    permission_required=('Logistica y Compras','Marketing y Ventas')
    model=ubicacion
    form_class=ubicacionForm
    login_url='login'
    redirect_field_name='next'
    success_url = reverse_lazy('erp:inmueble')
    def get_context_data(self, **kwargs):
        context=super().get_context_data(**kwargs)
        context['title']='Crear Barrio'
        return context

class tipoinmuebleCreateView(LoginRequiredMixin,ValidatePermissionRequiredMixin,CreateView):
    permission_required=('Logistica y Compras','Marketing y Ventas')
    model=tipos_inmuebles
    form_class=tipoinmuebleForm
    login_url='login'
    redirect_field_name='next'
    success_url = reverse_lazy('erp:inmueble')
    def get_context_data(self, **kwargs):
        context=super().get_context_data(**kwargs)
        context['title']='Crear Tipo de Inmueble'
        return context

class serviciosCreateView(LoginRequiredMixin,ValidatePermissionRequiredMixin,CreateView):
    permission_required=('Logistica y Compras','Marketing y Ventas')
    model=tipos_negocios
    form_class=tipo_negocioForm
    login_url='login'
    redirect_field_name='next'
    success_url = reverse_lazy('erp:inmueble')
    def get_context_data(self, **kwargs):
        context=super().get_context_data(**kwargs)
        context['title']='Crear Servicio'
        return context


class fichamodalListView(LoginRequiredMixin,ValidatePermissionRequiredMixin,ListView):
    permission_required=('Logistica y Compras','Marketing y Ventas')
    model = inmuebles
    template_name = 'inmuebles/list.html'
    login_url='login'
    redirect_field_name='next'
    
    @method_decorator(csrf_exempt) 
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def post(self, request, *args, **kwargs):
        action = request.POST['action']  
        try: 
            if action == 'searchall':
                inmueble=inmuebles.objects.all()
                inmueble_serializer=InmueblesSerializer(inmueble,many=True)
            elif action == 'fecha':
                contrato= request.POST['contrato']
                estatus=request.POST['estatus']
                if contrato !="no" and estatus!="no":
                    inmueble=inmuebles.objects.filter(estatus=estatus,tipo_negocio_id=int(contrato))
                    inmueble_serializer=InmueblesSerializer(inmueble,many=True)
                elif contrato =="no" and estatus!="no":
                    inmueble=inmuebles.objects.filter(estatus=estatus)
                    inmueble_serializer=InmueblesSerializer(inmueble,many=True)
                elif contrato !="no" and estatus =="no":
                    inmueble=inmuebles.objects.filter(tipo_negocio_id=int(contrato))
                    inmueble_serializer=InmueblesSerializer(inmueble,many=True)        
        except Exception as e:
            data['error'] = str(e)
        
        return JsonResponse(inmueble_serializer.data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado Inmuebles'
        context['create_url'] = reverse_lazy('detalle')
        context['list_url'] = reverse_lazy('detalle')
        context['entity'] = 'detalles'
        return context 
   


class InmuebleUpdateView(LoginRequiredMixin,ValidatePermissionRequiredMixin,UpdateView):
    model=inmuebles
    form_class=inventarioForm
    template_name='inmuebles/FormularioInmuebles.html'
    success_url = reverse_lazy('erp:fichamodal')
    permission_required = 'erp.fichamodal'
    url_redirect = success_url
    permission_required=('Logistica y Compras','Marketing y Ventas')
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)
    @method_decorator(csrf_exempt) 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de un Inmueble'
        context['entity'] = 'Inmuebles'
        context['list_url'] = self.success_url
        context['action'] = 'edit'
        return context


class tipo_clienteCreateView(LoginRequiredMixin,ValidatePermissionRequiredMixin,CreateView):
    permission_required=('Logistica y Compras','Marketing y Ventas')
    model=tipo_clentes
    form_class=tipo_clienteForm
    template_name='inmuebles/Formulariotipo.html'
    def get_context_data(self, **kwargs):
        context=super().get_context_data(**kwargs)
        context['title']='Crear Tipo'
        return context


class AdministracionListView(LoginRequiredMixin,ValidatePermissionRequiredMixin,ListView):
    permission_required = ('Marketing y Ventas')
    model = contratos
    template_name = 'inmuebles/listadministracion.html'
    
    login_url='login'
    redirect_field_name='next'
    @method_decorator(csrf_exempt) 
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def post(self, request, *args, **kwargs):
        id = self.kwargs.get('id', None) 
        
        data = {}
        
        try:
            action = request.POST['action']
            contrato=contratos.objects.filter(codigo="CD")
            contrato_serializer=contratosSerializer(contrato,many=True)
                # data = []
                
                # for i in contratos.objects.all():
                #     data.append(i.toJSON())
                    
            if action=='add':
                cli=clientes()
                cli.cedula=request.POST['cedula']
                cli.nombre=request.POST['nombre']
                cli.correo_electronico=request.POST['correo_electronico']
                cli.telefono=request.POST['telefono']
                cli.tipo_cliente_id=request.POST['tipo_cliente']
                cli.tipo_persona=request.POST['tipo_persona']
                cli.ciudad_domicilo=request.POST['ciudad_domicilo']
                cli.ciudad_expedicion=request.POST['ciudad_expedicion']
                cli.direccion=request.POST['direccion']
                cli.genero=request.POST['genero']
                cli.save()
            elif action=='add1':
                cli=codeudores()
                cli.cedula=request.POST['cedula']
                cli.nombre=request.POST['nombre']
                cli.correo_electronico=request.POST['correo_electronico']
                cli.telefono=request.POST['telefono']
                cli.tipo_persona=request.POST['tipo_persona']
                cli.ciudad_domicilo=request.POST['ciudad_domicilo']
                cli.ciudad_expedicion=request.POST['ciudad_expedicion']
                cli.direccion=request.POST['direccion']
                cli.genero=request.POST['genero']
                cli.save()
            elif action=='add2':
                cli=representantes()
                cli.cedula=request.POST['cedula']
                cli.nombre=request.POST['nombre']
                cli.correo_electronico=request.POST['correo_electronico']
                cli.telefono=request.POST['telefono']
                cli.tipo_persona=request.POST['tipo_persona']
                cli.ciudad_domicilo=request.POST['ciudad_domicilo']
                cli.ciudad_expedicion=request.POST['ciudad_expedicion']
                cli.direccion=request.POST['direccion']
                cli.genero=request.POST['genero']
                cli.save()
            elif action=='add3':
                contrato=contratos.objects.filter(tipo_contrato=request.POST['tipo_contrato']).last()
                if contrato:
                    fecha=fecha = datetime.strptime(request.POST['fecha'], "%Y-%m-%d")
                    fecha1=contrato.fecha
                    if fecha.month==fecha1.month:
                        numeroconm=contrato.numero_mensual+1
                    elif fecha.month != fecha1.month:
                        numeroconm=1
                    if fecha.year==fecha1.year:
                        numerocona=contrato.numero_anual+1
                    elif fecha.year != fecha1.year:
                        numerocona=1
                else:
                    numerocona=1
                    numeroconm=1
                contr=inmuebles.objects.get(id=request.POST['inmueble'])
                cli=contratos()
                cli.numero_mensual=numeroconm
                cli.numero_anual=numerocona
                cli.consecutivo=consecutivos(request.POST['fecha'],numeroconm,numerocona)
                cli.codigo=codigo='CD'
                cli.referencia=ref(request.POST['inmueble'])
                cli.inmueble_id=request.POST['inmueble']
                cli.precio=contr.precio_alquiler
                cli.duracion_contrato=request.POST['duracion_contrato']
                cli.fecha=request.POST['fecha']
                cli.arrendatario_id=contr.clientes_id
                cli.tipo_contrato=request.POST['tipo_contrato']
                cli.categorias_id=contr.categorias_id
                cli.save()
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(contrato_serializer.data, safe=False)

    def get_context_data(self, **kwargs):
        nombre = self.kwargs.get('nom', None)
        cedula = self.kwargs.get('id', None)
        context = super().get_context_data(**kwargs)
        context['title'] = 'Administracion'
        context['name'] = nombre
        context['id'] = cedula
        context['create_url'] = reverse_lazy('contratoslist')
        context['list_url'] = reverse_lazy('contratoslist')
        context['entity'] = 'contratos'
        context['forma'] = Clientes_inmoviliariaForm
        context['formr'] = RepresentanteForm
        context['formadmin'] = administracionForm
        context['formcod'] = codeudorForm
        return context    


class CorretajeListView(LoginRequiredMixin,ValidatePermissionRequiredMixin,ListView):
    permission_required = ('Marketing y Ventas')
    model = contratos
    template_name = 'inmuebles/listcorretaje.html'
    
    login_url='login'
    redirect_field_name='next'
    @method_decorator(csrf_exempt) 
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def post(self, request, *args, **kwargs):
        id = self.kwargs.get('id', None) 
        
        data = {}
        
      #  try:

        action = request.POST['action']
        contrato=contratos.objects.filter(codigo="CC")
        contrato_serializer=contratosSerializer(contrato,many=True)
                # data = []
                
                # for i in contratos.objects.all():
                #     data.append(i.toJSON())
                    
        if action=='add':
                cli=clientes()
                cli.cedula=request.POST['cedula']
                cli.nombre=request.POST['nombre']
                cli.correo_electronico=request.POST['correo_electronico']
                cli.telefono=request.POST['telefono']
                cli.tipo_cliente_id=request.POST['tipo_cliente']
                cli.tipo_persona=request.POST['tipo_persona']
                cli.ciudad_domicilo=request.POST['ciudad_domicilo']
                cli.ciudad_expedicion=request.POST['ciudad_expedicion']
                cli.direccion=request.POST['direccion']
                cli.genero=request.POST['genero']
                cli.save()
        elif action=='add3':
                contrato=contratos.objects.filter(tipo_contrato=request.POST['tipo_contrato']).last()
                if contrato:
                    fecha=fecha = datetime.strptime(request.POST['fecha'], "%Y-%m-%d")
                    fecha1=contrato.fecha
                    if fecha.month==fecha1.month:
                        numeroconm=contrato.numero_mensual+1
                    elif fecha.month != fecha1.month:
                        numeroconm=1
                    if fecha.year==fecha1.year:
                        numerocona=contrato.numero_anual+1
                    elif fecha.year != fecha1.year:
                        numerocona=1
                else:
                    numerocona=1
                    numeroconm=1
                contr=inmuebles.objects.get(id=request.POST['inmueble'])
                cli=contratos()
                cli.numero_mensual=numeroconm
                cli.numero_anual=numerocona
                cli.consecutivo=consecutivos(request.POST['fecha'],numeroconm,numerocona)
                cli.codigo=codigo='CC'
                cli.referencia=ref(request.POST['inmueble'])
                cli.inmueble_id=request.POST['inmueble']
                cli.precio=contr.precio_venta
                cli.duracion_contrato=request.POST['duracion_contrato']
                cli.fecha=request.POST['fecha']
                cli.arrendatario_id=contr.clientes_id
                cli.tipo_contrato=request.POST['tipo_contrato']
                cli.categorias_id=contr.categorias_id
                cli.save()
        else:
            data['error'] = 'Ha ocurrido un error'
       # except Exception as e:
        #    data['error'] = str(e)
        return JsonResponse(contrato_serializer.data, safe=False)

    def get_context_data(self, **kwargs):
        nombre = self.kwargs.get('nom', None)
        cedula = self.kwargs.get('id', None)
        context = super().get_context_data(**kwargs)
        context['title'] = 'Corretaje'
        context['name'] = nombre
        context['id'] = cedula
        context['create_url'] = reverse_lazy('contratoslist')
        context['list_url'] = reverse_lazy('contratoslist')
        context['entity'] = 'contratos'
        context['forma'] = Clientes_inmoviliariaForm
        context['formadmin'] = administracionForm
        return context    


class ArrendamientoListView(LoginRequiredMixin,ValidatePermissionRequiredMixin,ListView):
    permission_required = ('Marketing y Ventas')
    model = contratos
    template_name = 'inmuebles/listarrendamiento.html'
    
    login_url='login'
    redirect_field_name='next'
    @method_decorator(csrf_exempt) 
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
    
    def post(self, request, *args, **kwargs):
        id = self.kwargs.get('id', None) 
        
        data = {}
        
        try:
            action = request.POST['action']
            contrato=contratos.objects.filter(codigo="CA")
            contrato_serializer=contratosSerializer(contrato,many=True)
                # data = []
                
                # for i in contratos.objects.all():
                #     data.append(i.toJSON())
                    
            if action=='add':
                cli=clientes()
                cli.cedula=request.POST['cedula']
                cli.nombre=request.POST['nombre']
                cli.correo_electronico=request.POST['correo_electronico']
                cli.telefono=request.POST['telefono']
                cli.tipo_cliente_id=request.POST['tipo_cliente']
                cli.tipo_persona=request.POST['tipo_persona']
                cli.ciudad_domicilo=request.POST['ciudad_domicilo']
                cli.ciudad_expedicion=request.POST['ciudad_expedicion']
                cli.direccion=request.POST['direccion']
                cli.genero=request.POST['genero']
                cli.save()
            elif action=='add1':
                cli=codeudores()
                cli.cedula=request.POST['cedula']
                cli.nombre=request.POST['nombre']
                cli.correo_electronico=request.POST['correo_electronico']
                cli.telefono=request.POST['telefono']
                cli.tipo_persona=request.POST['tipo_persona']
                cli.ciudad_domicilo=request.POST['ciudad_domicilo']
                cli.ciudad_expedicion=request.POST['ciudad_expedicion']
                cli.direccion=request.POST['direccion']
                cli.genero=request.POST['genero']
                cli.save()
            elif action=='add2':
                cli=representantes()
                cli.cedula=request.POST['cedula']
                cli.nombre=request.POST['nombre']
                cli.correo_electronico=request.POST['correo_electronico']
                cli.telefono=request.POST['telefono']
                cli.tipo_persona=request.POST['tipo_persona']
                cli.ciudad_domicilo=request.POST['ciudad_domicilo']
                cli.ciudad_expedicion=request.POST['ciudad_expedicion']
                cli.direccion=request.POST['direccion']
                cli.genero=request.POST['genero']
                cli.save()
            elif action=='add3':
                contrato=contratos.objects.filter(tipo_contrato=request.POST['tipo_contrato']).last()
                if contrato:
                    fecha=fecha = datetime.strptime(request.POST['fecha'], "%Y-%m-%d")
                    fecha1=contrato.fecha
                    if fecha.month==fecha1.month:
                        numeroconm=contrato.numero_mensual+1
                    elif fecha.month != fecha1.month:
                        numeroconm=1
                    if fecha.year==fecha1.year:
                        numerocona=contrato.numero_anual+1
                    elif fecha.year != fecha1.year:
                        numerocona=1
                else:
                    numerocona=1
                    numeroconm=1
                contr=inmuebles.objects.get(id=request.POST['inmueble'])
                canon=contr.precio_alquiler
                cli=contratos()
                cli.numero_mensual=numeroconm
                cli.numero_anual=numerocona
                cli.consecutivo=consecutivos(request.POST['fecha'],numeroconm,numerocona)
                cli.codigo=codigo='CA'
                cli.referencia=ref(request.POST['inmueble'])
                cli.inmueble_id=request.POST['inmueble']
                cli.duracion_contrato=request.POST['duracion_contrato']
                cli.fecha=request.POST['fecha']
                cli.arrendatario_id=request.POST['arrendatario']
                cli.codeudor_id=request.POST['codeudor']
                cli.tipo_contrato=request.POST['tipo_contrato']
                cli.categorias_id=contr.categorias_id
                cli.precio=canon
                cli.compañia_id=request.POST['compañia']
                cli.save()
            elif action=='add4':
                cli=compañias()
                cli.nit=request.POST['nit']
                cli.razon_social=request.POST['razon_social']
                cli.telefono=request.POST['telefono']
                cli.direccion=request.POST['direccion']
                cli.ciudad=request.POST['ciudad']
                cli.cliente_id=request.POST['cliente']
                cli.save()          
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(contrato_serializer.data, safe=False)

    def get_context_data(self, **kwargs):
        nombre = self.kwargs.get('nom', None)
        cedula = self.kwargs.get('id', None)
        context = super().get_context_data(**kwargs)
        context['title'] = 'Arrendamiento'
        context['name'] = nombre
        context['id'] = cedula
        context['create_url'] = reverse_lazy('contratoslist')
        context['list_url'] = reverse_lazy('contratoslist')
        context['entity'] = 'contratos'
        context['forma'] = Clientes_inmoviliariaForm
        context['formr'] = RepresentanteForm
        context['formadmin'] = arrendamientoForm
        context['formcod'] = codeudorForm
        context['formcompañia'] = compañiasForm
        return context    

class AdministracionUpdateView(LoginRequiredMixin,ValidatePermissionRequiredMixin,UpdateView):
    model=cuerpoContrato
    form_class=editcontratoForm
    template_name='inmuebles/update.html'
    success_url = reverse_lazy('erp:administracionlist')
    permission_required = 'erp.administracionlist'
    url_redirect = success_url
    permission_required='Marketing y Ventas'
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)
    @method_decorator(csrf_exempt) 
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Editar Contrato'
        context['entity'] = 'cuerpoContrato'
        context['list_url'] = self.success_url
        context['action'] = 'edit'
        return context

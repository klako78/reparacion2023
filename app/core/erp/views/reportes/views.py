from django.urls import reverse_lazy
from django.http import HttpResponse,HttpResponseRedirect
from django.views.generic import *
from core.erp.models import *
from core.erp.forms import *
import os
from django.conf import settings
from django.template.loader import get_template
from xhtml2pdf import pisa
from django.views.generic import View
from django.contrib.staticfiles import finders
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models.aggregates import Sum
from django.db.models import F
from datetime import datetime
from datetime import date
from core.erp.serializers import InmueblesSerializer
from core.erp.mixins import ValidatePermissionRequiredMixin
from core.erp.number_to_leter import *
from datetime import date, timedelta
# Create your views here.

class InformePDFView(LoginRequiredMixin,ValidatePermissionRequiredMixin,View):
    permission_required=('Logistica y Compras')
    login_url='login'
    redirect_field_name='next'
    def link_callback(self, uri, rel):
        """
        Convert HTML URIs to absolute system paths so xhtml2pdf can access those
        resources
        """
        # use short variable names
        sUrl = settings.STATIC_URL  # Typically /static/
        sRoot = settings.STATIC_ROOT  # Typically /home/userX/project_static/
        mUrl = settings.MEDIA_URL  # Typically /static/media/
        mRoot = settings.MEDIA_ROOT  # Typically /home/userX/project_static/media/

        # convert URIs to absolute system paths
        if uri.startswith(mUrl):
            path = os.path.join(mRoot, uri.replace(mUrl, ""))
        elif uri.startswith(sUrl):
            path = os.path.join(sRoot, uri.replace(sUrl, ""))
        else:
            return uri  # handle absolute uri (ie: http://some.tld/foo.png)

        # make sure that file exists
        if not os.path.isfile(path):
            raise Exception(
                'media URI must start with %s or %s' % (sUrl, mUrl)
            )
        return path

    def get(self, request, *args, **kwargs):
        #  try:
            template = get_template('reportes/informe.html')
            id=self.kwargs['id']
            novedad=""
            visita=""
            presupuesto=""
            ejecucion=""
            entrega=""
            novedad=detalles_mantenimiento.objects.get(etapa='Novedad',id_mantenimiento=id)
            etapa=mantenimientos.objects.get(id=id)
            if etapa.etapa=="Inspección":
                visita=detalles_mantenimiento.objects.get(etapa='Inspección',id_mantenimiento=id)
            if etapa.etapa=="Presupuesto":
                visita=detalles_mantenimiento.objects.get(etapa='Inspección',id_mantenimiento=id)
                presupuesto=detalles_mantenimiento.objects.get(etapa='Presupuesto',id_mantenimiento=id)
            if etapa.etapa=="Ejecucion":
                visita=detalles_mantenimiento.objects.get(etapa='Inspección',id_mantenimiento=id)
                presupuesto=detalles_mantenimiento.objects.get(etapa='Presupuesto',id_mantenimiento=id) 
                ejecucion=detalles_mantenimiento.objects.filter(etapa='Ejecucion',id_mantenimiento=id)
            if etapa.etapa=="Entrega" and etapa.fecha_terminacion:
                visita=detalles_mantenimiento.objects.get(etapa='Inspección',id_mantenimiento=id)
                presupuesto=detalles_mantenimiento.objects.get(etapa='Presupuesto',id_mantenimiento=id) 
                ejecucion=detalles_mantenimiento.objects.filter(etapa='Ejecucion',id_mantenimiento=id)
                entrega=detalles_mantenimiento.objects.get(etapa='Entrega',id_mantenimiento=id)
            if etapa.etapa=="Entrega" and not etapa.fecha_terminacion:
                visita=detalles_mantenimiento.objects.get(etapa='Inspección',id_mantenimiento=id)
                presupuesto=detalles_mantenimiento.objects.get(etapa='Presupuesto',id_mantenimiento=id) 
                ejecucion=detalles_mantenimiento.objects.filter(etapa='Ejecucion',id_mantenimiento=id)
            inmueble=inmuebles.objects.get(id=novedad.inmueble)
            #contrato=contratos.objects.get(inmueble_id=inmueble.id,codigo="CA")
            
            context = {
                # 'arrendatario': contrato,
                'novedad': novedad,
                'visita': visita,
                'presupuesto': presupuesto,
                'inmueble': inmueble,
                'ejecucion': ejecucion,
                'entrega': entrega,
                'cons':  id,
                'comp': {'name': 'ALEMAN VERGARA PROYECTOS INMOBILIARIOS SAS', 'ruc': 'NIT: 900602576-9', 'address': 'DIRECCION: CLL 12 # 17 - 42 FONSECA, LA GUAJIRA','telefono':'TELEFONO: +57(605)7756666 – 318-5499250'},
                'logo': '{}{}'.format(settings.STATIC_URL, 'img/Logo.jpg')
            }
            html = template.render(context)
            response = HttpResponse(content_type='application/pdf')
            #response['Content-Disposition'] = 'attachment; filename="report.pdf"'
            pisaStatus = pisa.CreatePDF(
                html, dest=response,
                link_callback=self.link_callback
            )
            return response
        #  except:
        #     pass
        #  return HttpResponseRedirect(reverse_lazy('erp:mantenimientoslist'))


class ContratoAdminPDFView(LoginRequiredMixin,ValidatePermissionRequiredMixin,View):
    permission_required=('Marketing y Ventas')
    login_url='login'
    redirect_field_name='next'
    def link_callback(self, uri, rel):
        """
        Convert HTML URIs to absolute system paths so xhtml2pdf can access those
        resources
        """
        # use short variable names
        sUrl = settings.STATIC_URL  # Typically /static/
        sRoot = settings.STATIC_ROOT  # Typically /home/userX/project_static/
        mUrl = settings.MEDIA_URL  # Typically /static/media/
        mRoot = settings.MEDIA_ROOT  # Typically /home/userX/project_static/media/

        # convert URIs to absolute system paths
        if uri.startswith(mUrl):
            path = os.path.join(mRoot, uri.replace(mUrl, ""))
        elif uri.startswith(sUrl):
            path = os.path.join(sRoot, uri.replace(sUrl, ""))
        else:
            return uri  # handle absolute uri (ie: http://some.tld/foo.png)

        # make sure that file exists
        if not os.path.isfile(path):
            raise Exception(
                'media URI must start with %s or %s' % (sUrl, mUrl)
            )
        return path

    def get(self, request, *args, **kwargs):
         try:
            template = get_template('reportes/contratoadmin.html')
            id=self.kwargs['id']
            contrato=contratos.objects.get(id=id)
            codigo=contrato.codigo
            referencia=contrato.referencia
            consecutivo=contrato.consecutivo
            valor=contrato.precio
            duracion=contrato.duracion_contrato
            fecha=contrato.fecha
            cedula=contrato.arrendatario_id
            id_inm=contrato.inmueble_id
            cliente=clientes.objects.get(cedula=cedula)
            inmueble=inmuebles.objects.get(id=id_inm)
            linderos=inmueble.linderos
            valor1=numero_a_letras(valor)
            if cliente.genero=="Femenino":
                genero="identificada"
                propietario="propietaria"
                unico="única"
                domicilio="domiciliada"
                la="LA"
            if cliente.genero=="Masculino":
                genero="identificado"
                propietario="propietario"
                unico="único"
                domicilio="domiciliado"
                la="EL"
            if inmueble.categorias_id==2:
                el="el"
                una="un"
                ubicada="ubicado"
                inm=inmueble.categorias
            if inmueble.categorias_id==3:
                el="el"
                una="un"
                ubicada="ubicado"
                inm=inmueble.categorias
            if inmueble.categorias_id==6:
                el="la"
                una="una"
                ubicada="ubicada"
                inm="vivienda"
            if inmueble.categorias_id==7:
                el="la"
                una="una"
                ubicada="ubicada"
                inm="vivienda"
            if inmueble.categorias_id==4:
                el="la"
                una="una"
                ubicada="ubicada"
                inm=inmueble.categorias
            cuerpo=cuerpoContrato.objects.get(id=1)
            context = {
                'cuerpo': cuerpo.cuerpo,
                'la': la,
                'el': el,
                'domicilio': domicilio,
                'propietario': propietario,
                'unico': unico,
                'inmueble': inmueble,
                'codigo': codigo,
                'referencia': referencia,
                'consecutivo': consecutivo,
                'cliente': cliente,
                'linderos': linderos,
                'valor': '{:,.2f}'.format(valor).replace(",", "@").replace(".", ",").replace("@", "."),
                'valor1': valor1,
                'duracion': duracion,
                'fecha': fecha,
                'genero': genero,
                'una': una,
                'ubicada': ubicada,
                'inm': inm,
                'comp': {'name': 'ALEMAN VERGARA PROYECTOS INMOBILIARIOS SAS', 'ruc': 'NIT: 900602576-9', 'address': 'DIRECCION: CLL 12 # 17 - 42 FONSECA, LA GUAJIRA','telefono':'TELEFONO: 7756344 – 3168347504'},
                'logo': '{}{}'.format(settings.STATIC_URL, 'img/Logo.jpg')
            }
            html = template.render(context)
            response = HttpResponse(content_type='application/pdf')
            #response['Content-Disposition'] = 'attachment; filename="report.pdf"'
            pisaStatus = pisa.CreatePDF(
                html, dest=response,
                link_callback=self.link_callback
            )
            return response
         except:
            pass
         return HttpResponseRedirect(reverse_lazy('erp:administracionlist'))

class ContratoCorretajePDFView(LoginRequiredMixin,ValidatePermissionRequiredMixin,View):
    permission_required=('Marketing y Ventas')
    login_url='login'
    redirect_field_name='next'
    def link_callback(self, uri, rel):
        """
        Convert HTML URIs to absolute system paths so xhtml2pdf can access those
        resources
        """
        # use short variable names
        sUrl = settings.STATIC_URL  # Typically /static/
        sRoot = settings.STATIC_ROOT  # Typically /home/userX/project_static/
        mUrl = settings.MEDIA_URL  # Typically /static/media/
        mRoot = settings.MEDIA_ROOT  # Typically /home/userX/project_static/media/

        # convert URIs to absolute system paths
        if uri.startswith(mUrl):
            path = os.path.join(mRoot, uri.replace(mUrl, ""))
        elif uri.startswith(sUrl):
            path = os.path.join(sRoot, uri.replace(sUrl, ""))
        else:
            return uri  # handle absolute uri (ie: http://some.tld/foo.png)

        # make sure that file exists
        if not os.path.isfile(path):
            raise Exception(
                'media URI must start with %s or %s' % (sUrl, mUrl)
            )
        return path

    def get(self, request, *args, **kwargs):
        #  try:
            template = get_template('reportes/contratocorretaje.html')
            id=self.kwargs['id']
            contrato=contratos.objects.get(id=id)
            codigo=contrato.codigo
            referencia=contrato.referencia
            consecutivo=contrato.consecutivo
            duracion=contrato.duracion_contrato
            fecha=contrato.fecha
            cedula=contrato.arrendatario_id
            id_inm=contrato.inmueble_id
            cliente=clientes.objects.get(cedula=cedula)
            inmueble=inmuebles.objects.get(id=id_inm)
            valor=inmueble.precio_venta
            linderos=inmueble.linderos
            valor1=numero_a_letras(valor)
            if cliente.genero=="Femenino":
                genero="identificada"
                propietario="propietaria"
                unico="única"
                domicilio="domiciliada"
                la="LA"
            if cliente.genero=="Masculino":
                genero="identificado"
                propietario="propietario"
                unico="único"
                domicilio="domiciliado"
                la="EL"
            if inmueble.categorias_id==2:
                el="el"
                una="un"
                ubicada="ubicado"
                inm=inmueble.categorias
            if inmueble.categorias_id==3:
                el="el"
                una="un"
                ubicada="ubicado"
                inm=inmueble.categorias
            if inmueble.categorias_id==11:
                el='el'
                una='un'  
                ubicada='ubicado'
                inm=inmueble.categorias
            if inmueble.categorias_id==6:
                el="la"
                una="una"
                ubicada="ubicada"
                inm="vivienda"
            if inmueble.categorias_id==7:
                el="la"
                una="una"
                ubicada="ubicada"
                inm="vivienda"
            if inmueble.categorias_id==4:
                el="la"
                una="una"
                ubicada="ubicada"
                inm=inmueble.categorias
            cuerpo=cuerpoContrato.objects.get(id=2)
            cuerpo1=cuerpoContrato.objects.get(id=3)
            context = {
                'cuerpo': cuerpo.cuerpo,
                'cuerpo1': cuerpo1.cuerpo,
                'la': la,
                'el': el,
                'domicilio': domicilio,
                'propietario': propietario,
                'unico': unico,
                'inmueble': inmueble,
                'codigo': codigo,
                'referencia': referencia,
                'consecutivo': consecutivo,
                'cliente': cliente,
                'linderos': linderos,
                'valor': '{:,.2f}'.format(valor).replace(",", "@").replace(".", ",").replace("@", "."),
                'valor1': valor1,
                'duracion': duracion,
                'fecha': fecha,
                'genero': genero,
                'una': una,
                'ubicada': ubicada,
                'inm': inm,
                'comp': {'name': 'ALEMAN VERGARA PROYECTOS INMOBILIARIOS SAS', 'ruc': 'NIT: 900602576-9', 'address': 'DIRECCION: CLL 12 # 17 - 42 FONSECA, LA GUAJIRA','telefono':'TELEFONO: 7756344 – 3168347504'},
                'logo': '{}{}'.format(settings.STATIC_URL, 'img/Logo.jpg')
            }
            html = template.render(context)
            response = HttpResponse(content_type='application/pdf')
            #response['Content-Disposition'] = 'attachment; filename="report.pdf"'
            pisaStatus = pisa.CreatePDF(
                html, dest=response,
                link_callback=self.link_callback
            )
            return response
        #  except:
            # pass
        #  return HttpResponseRedirect(reverse_lazy('erp:corretajelist'))



class ContratoArendamientoPDFView(LoginRequiredMixin,ValidatePermissionRequiredMixin,View):
    permission_required=('Marketing y Ventas')
    login_url='login'
    redirect_field_name='next'
    def link_callback(self, uri, rel):
        """
        Convert HTML URIs to absolute system paths so xhtml2pdf can access those
        resources
        """
        # use short variable names
        sUrl = settings.STATIC_URL  # Typically /static/
        sRoot = settings.STATIC_ROOT  # Typically /home/userX/project_static/
        mUrl = settings.MEDIA_URL  # Typically /static/media/
        mRoot = settings.MEDIA_ROOT  # Typically /home/userX/project_static/media/

        # convert URIs to absolute system paths
        if uri.startswith(mUrl):
            path = os.path.join(mRoot, uri.replace(mUrl, ""))
        elif uri.startswith(sUrl):
            path = os.path.join(sRoot, uri.replace(sUrl, ""))
        else:
            return uri  # handle absolute uri (ie: http://some.tld/foo.png)

        # make sure that file exists
        if not os.path.isfile(path):
            raise Exception(
                'media URI must start with %s or %s' % (sUrl, mUrl)
            )
        return path

    def get(self, request, *args, **kwargs):
         try:
            template = get_template('reportes/contratoarrendamiento.html')
            id=self.kwargs['id']
            contrato=contratos.objects.get(id=id)
            codigo=contrato.codigo
            referencia=contrato.referencia
            consecutivo=contrato.consecutivo
            duracion=contrato.duracion_contrato
            valor=contrato.precio
            fecha=contrato.fecha
            cedula=contrato.arrendatario_id
            if contrato.compañia:
                 empresa=compañias.objects.get(id=contrato.compañia_id)
                 representante=clientes.objects.get(cedula=empresa.cliente_id)
            else:
                empresa=""
                representante=""

            if contrato.codeudor:
                deudorS=contrato.codeudor_id
                codeudor=codeudores.objects.get(cedula=deudorS)
            else:
                codeudor=""
            cliente=clientes.objects.get(cedula=cedula)
            inmueble=inmuebles.objects.get(id=contrato.inmueble_id)
            linderos=inmueble.linderos
            valor1=numero_a_letras(valor)
            if cliente.genero=="Femenino":
                genero="identificada"
                propietario="propietaria"
                unico="única"
                domicilio="domiciliada"
                la="LA"
            if cliente.genero=="Masculino":
                genero="identificado"
                propietario="propietario"
                unico="único"
                domicilio="domiciliado"
                la="EL"
            if inmueble.categorias_id==2 or inmueble.categorias_id==10:
                el="el"
                una="un"
                ubicada="ubicado"
                inm=inmueble.categorias
            if inmueble.categorias_id==3:
                el="el"
                una="un"
                ubicada="ubicado"
                inm=inmueble.categorias
            if inmueble.categorias_id==6:
                el="la"
                una="una"
                ubicada="ubicada"
                inm="vivienda"
            if inmueble.categorias_id==7:
                el="la"
                una="una"
                ubicada="ubicada"
                inm="vivienda"
            if inmueble.categorias_id==4:
                el="la"
                una="una"
                ubicada="ubicada"
                inm=inmueble.categorias
            cuerpo=cuerpoContrato.objects.get(id=4)
            context = {
                'representante': representante,
                'empresa': empresa,
                'cuerpo': cuerpo.cuerpo,
                'la': la,
                'el': el,
                'domicilio': domicilio,
                'codeudor': codeudor,
                'propietario': propietario,
                'unico': unico,
                'inmueble': inmueble,
                'codigo': codigo,
                'referencia': referencia,
                'consecutivo': consecutivo,
                'cliente': cliente,
                'linderos': linderos,
                'valor': '{:,.2f}'.format(valor).replace(",", "@").replace(".", ",").replace("@", "."),
                'valor1': valor1,
                'duracion': duracion,
                'fecha': fecha,
                'genero': genero,
                'una': una,
                'ubicada': ubicada,
                'inm': inm,
                'comp': {'name': 'ALEMAN VERGARA PROYECTOS INMOBILIARIOS SAS', 'ruc': 'NIT: 900602576-9', 'address': 'DIRECCION: CLL 12 # 17 - 42 FONSECA, LA GUAJIRA','telefono':'TELEFONO: 7756344 – 3168347504'},
                'logo': '{}{}'.format(settings.STATIC_URL, 'img/Logo.jpg')
            }
            html = template.render(context)
            response = HttpResponse(content_type='application/pdf')
            #response['Content-Disposition'] = 'attachment; filename="report.pdf"'
            pisaStatus = pisa.CreatePDF(
                html, dest=response,
                link_callback=self.link_callback
            )
            return response
         except:
            pass
         return HttpResponseRedirect(reverse_lazy('erp:arrendamientolist'))


class InformeventasPDFView(LoginRequiredMixin,ValidatePermissionRequiredMixin,View):
    permission_required=('Ferreteria','Administrador')
    login_url='login'
    redirect_field_name='next'
    def link_callback(self, uri, rel):
        """
        Convert HTML URIs to absolute system paths so xhtml2pdf can access those
        resources
        """
        # use short variable names
        sUrl = settings.STATIC_URL  # Typically /static/
        sRoot = settings.STATIC_ROOT  # Typically /home/userX/project_static/
        mUrl = settings.MEDIA_URL  # Typically /static/media/
        mRoot = settings.MEDIA_ROOT  # Typically /home/userX/project_static/media/

        # convert URIs to absolute system paths
        if uri.startswith(mUrl):
            path = os.path.join(mRoot, uri.replace(mUrl, ""))
        elif uri.startswith(sUrl):
            path = os.path.join(sRoot, uri.replace(sUrl, ""))
        else:
            return uri  # handle absolute uri (ie: http://some.tld/foo.png)

        # make sure that file exists
        if not os.path.isfile(path):
            raise Exception(
                'media URI must start with %s or %s' % (sUrl, mUrl)
            )
        return path
   
    def get(self, request, *args, **kwargs):
        
        try:
            template = get_template('reportes/informeventas.html')
            start=self.kwargs['init']
            end=self.kwargs['end']
            inicio=""
            fin=""
            if start=="Informe General":
                ventas_general=clientes.objects.values("nombre","cedula").annotate(totalf=Sum('puntaje__total_factura'),totalp=Sum('puntaje__puntaje_total'),totalc=Sum('puntaje__canjeados'),totald=(F("totalp")-F("totalc")))
                sum=puntaje.objects.all().aggregate(total=Sum('total_factura'))
            else:
                ventas_general=clientes.objects.values("nombre","cedula").filter(puntaje__fecha__range=[start,end]).annotate(totalf=Sum('puntaje__total_factura'),totalp=Sum('puntaje__puntaje_total'),totalc=Sum('puntaje__canjeados'),totald=(F("totalp")-F("totalc")))
                sum=puntaje.objects.filter(fecha__range=[start,end]).aggregate(total=Sum('total_factura'))    
                inicio=datetime.strptime(start,'%Y-%m-%d')
                fin=datetime.strptime(end,'%Y-%m-%d')
           
            usuario_actual=request.user.first_name +" "+ request.user.last_name
            total=sum['total'] 
                          
            context = {
                'start': inicio,
                'end': fin,
                'ventas': ventas_general,
                'usuario': usuario_actual,
                'total': '{:,.2f}'.format(total).replace(",", "@").replace(".", ",").replace("@", "."),
                'comp': {'name': 'JEFFREY JEAN OTTO ALEMAN VERGARA', 'ruc': 'CC: 17951854', 'address': 'DIRECCION: CLL 15 # 17 - 101 FONSECA, LA GUAJIRA','telefono':'TELEFONO: 3176427614'},
                'logo': '{}{}'.format(settings.STATIC_URL, 'img/logoferreteria.jpg')
            }
            html = template.render(context)
            response = HttpResponse(content_type='application/pdf')
            #response['Content-Disposition'] = 'attachment; filename="report.pdf"'
            pisaStatus = pisa.CreatePDF(
                html, dest=response,
                link_callback=self.link_callback
            )
            return response
        except:
             pass
        return HttpResponseRedirect(reverse_lazy('erp:ventas'))


class InformeclientesPDFView(LoginRequiredMixin,ValidatePermissionRequiredMixin,View):
    permission_required=('Ferreteria','Administrador')
    login_url='login'
    redirect_field_name='next'
    def link_callback(self, uri, rel):
        """
        Convert HTML URIs to absolute system paths so xhtml2pdf can access those
        resources
        """
        # use short variable names
        sUrl = settings.STATIC_URL  # Typically /static/
        sRoot = settings.STATIC_ROOT  # Typically /home/userX/project_static/
        mUrl = settings.MEDIA_URL  # Typically /static/media/
        mRoot = settings.MEDIA_ROOT  # Typically /home/userX/project_static/media/

        # convert URIs to absolute system paths
        if uri.startswith(mUrl):
            path = os.path.join(mRoot, uri.replace(mUrl, ""))
        elif uri.startswith(sUrl):
            path = os.path.join(sRoot, uri.replace(sUrl, ""))
        else:
            return uri  # handle absolute uri (ie: http://some.tld/foo.png)

        # make sure that file exists
        if not os.path.isfile(path):
            raise Exception(
                'media URI must start with %s or %s' % (sUrl, mUrl)
            )
        return path
   
    def get(self, request, *args, **kwargs):
        
       # try:
            template = get_template('reportes/informeclientes.html')
            id=self.kwargs['id']
            puntos_canjeados=puntaje.objects.filter(cliente=id,operacion="CANJE")
            puntos_acumulados=puntaje.objects.filter(cliente=id,operacion="PUNTOS")
            cliente=clientes.objects.get(cedula=id)
            if puntos_canjeados:
                sum1=puntaje.objects.filter(cliente=id,operacion="CANJE").aggregate(total1=Sum('canjeados'))
                total1=sum1['total1']
            else:
                total1=0
            sum=puntaje.objects.filter(cliente=id,operacion="PUNTOS").aggregate(total=Sum('puntaje_total'))
            usuario_actual=request.user.first_name +" "+ request.user.last_name
            total=sum['total'] 
            puntos=puntaje.objects.filter(cliente=id,operacion="PUNTOS").last()
            fin=puntos.fecha
            inicio = fin - timedelta(days=60)
            rang_fecha=puntaje.objects.filter(fecha__range=[inicio,fin]).filter(cliente_id=id).aggregate(totalf=Sum("total_factura"))
            suma=rang_fecha['totalf']
            if suma <= 7000000:
                nivel=1
            elif suma > 7000000 and suma <= 10000000:
                nivel=2
            else:
                nivel=3                
            context = {
                'nivel': nivel,
                'cliente': cliente,
                'puntosA': puntos_acumulados,
                'puntosC': puntos_canjeados,
                'usuario': usuario_actual,
                'total': total,
                'total1': total1,
                'resta': total-total1,
                'comp': {'name': 'JEFFREY JEAN OTTO ALEMAN VERGARA', 'ruc': 'CC: 17951854', 'address': 'DIRECCION: CLL 15 # 17 - 101 FONSECA, LA GUAJIRA','telefono':'TELEFONO: 3176427614'},
                'logo': '{}{}'.format(settings.STATIC_URL, 'img/logoferreteria.jpg')
            }
            html = template.render(context)
            response = HttpResponse(content_type='application/pdf')
            #response['Content-Disposition'] = 'attachment; filename="report.pdf"'
            pisaStatus = pisa.CreatePDF(
                html, dest=response,
                link_callback=self.link_callback
            )
            return response
       # except:
        #     pass
        #return HttpResponseRedirect(reverse_lazy('erp:ventas'))


class InformenovedadPDFView(LoginRequiredMixin,ValidatePermissionRequiredMixin,View):
    permission_required=('Logistica y Compras')
    login_url='login'
    redirect_field_name='next'
    def link_callback(self, uri, rel):
        """
        Convert HTML URIs to absolute system paths so xhtml2pdf can access those
        resources
        """
        # use short variable names
        sUrl = settings.STATIC_URL  # Typically /static/
        sRoot = settings.STATIC_ROOT  # Typically /home/userX/project_static/
        mUrl = settings.MEDIA_URL  # Typically /static/media/
        mRoot = settings.MEDIA_ROOT  # Typically /home/userX/project_static/media/

        # convert URIs to absolute system paths
        if uri.startswith(mUrl):
            path = os.path.join(mRoot, uri.replace(mUrl, ""))
        elif uri.startswith(sUrl):
            path = os.path.join(sRoot, uri.replace(sUrl, ""))
        else:
            return uri  # handle absolute uri (ie: http://some.tld/foo.png)

        # make sure that file exists
        if not os.path.isfile(path):
            raise Exception(
                'media URI must start with %s or %s' % (sUrl, mUrl)
            )
        return path
   
    def get(self, request, *args, **kwargs):
        
        try:
            template = get_template('reportes/informenovedad.html')
            start=self.kwargs['init']
            end=self.kwargs['end']
            estado=self.kwargs['estado']
            inicio=""
            fin=""
            if start=="NONAME" or end=="Informe General":
                start="Informe General"
                
            if start=="Informe General" and estado == "no" :
                novedad_general=novedades.objects.all
                inicio="Informe General"
                estado="Informe General"
            elif start != "Informe General" and estado == "no":
                novedad_general=novedades.objects.filter(fecha_novedad__range=[start,end])
                inicio=datetime.strptime(start,'%Y-%m-%d')
                fin=datetime.strptime(end,'%Y-%m-%d')
            elif start == "Informe General" and estado != "no":
                novedad_general=novedades.objects.filter(estado_novedad=estado)
                
            else:     
                novedad_general=novedades.objects.filter(estado_novedad=estado,fecha_novedad__range=[start,end])
                inicio=datetime.strptime(start,'%Y-%m-%d')
                fin=datetime.strptime(end,'%Y-%m-%d')
            usuario_actual=request.user.first_name +" "+ request.user.last_name
                                   
            context = {
                'start': inicio,
                'end': fin,
                'novedad': novedad_general,
                'usuario': usuario_actual,
                'comp': {'name': 'ALEMAN VERGARA PROYECTOS INMOBILIARIOS SAS', 'ruc': 'NIT: 900602576-9', 'address': 'DIRECCION: CLL 12 # 17 - 42 FONSECA, LA GUAJIRA','telefono':'TELEFONO: +57(317)6427614 – 3168347504'},
                'logo': '{}{}'.format(settings.STATIC_URL, 'img/Logo.jpg')
            }
            html = template.render(context)
            response = HttpResponse(content_type='application/pdf')
            #response['Content-Disposition'] = 'attachment; filename="report.pdf"'
            pisaStatus = pisa.CreatePDF(
                html, dest=response,
                link_callback=self.link_callback
            )
            return response
        except:
             pass
        return HttpResponseRedirect(reverse_lazy('erp:ventas'))


class InformemantenimientoPDFView(LoginRequiredMixin,ValidatePermissionRequiredMixin,View):
    permission_required=('Logistica y Compras')
    login_url='login'
    redirect_field_name='next'
    def link_callback(self, uri, rel):
        """
        Convert HTML URIs to absolute system paths so xhtml2pdf can access those
        resources
        """
        # use short variable names
        sUrl = settings.STATIC_URL  # Typically /static/
        sRoot = settings.STATIC_ROOT  # Typically /home/userX/project_static/
        mUrl = settings.MEDIA_URL  # Typically /static/media/
        mRoot = settings.MEDIA_ROOT  # Typically /home/userX/project_static/media/

        # convert URIs to absolute system paths
        if uri.startswith(mUrl):
            path = os.path.join(mRoot, uri.replace(mUrl, ""))
        elif uri.startswith(sUrl):
            path = os.path.join(sRoot, uri.replace(sUrl, ""))
        else:
            return uri  # handle absolute uri (ie: http://some.tld/foo.png)

        # make sure that file exists
        if not os.path.isfile(path):
            raise Exception(
                'media URI must start with %s or %s' % (sUrl, mUrl)
            )
        return path
   
    def get(self, request, *args, **kwargs):
        
        try:
            template = get_template('reportes/informemantenimiento.html')
            start=self.kwargs['init']
            end=self.kwargs['end']
            estado=self.kwargs['estado']
            inicio=""
            fin=""
            if start=="NONAME" or end=="Informe General":
                start="Informe General"
                
            if start=="Informe General" and estado == "no" :
                mantenimiento=mantenimientos.objects.all
                inicio="Informe General"
                estado="Informe General"
            elif start != "Informe General" and estado == "no":
                mantenimiento=mantenimientos.objects.filter(fecha_inicio__range=[start,end])
                inicio=datetime.strptime(start,'%Y-%m-%d')
                fin=datetime.strptime(end,'%Y-%m-%d')
            elif start == "Informe General" and estado != "no":
                mantenimiento=mantenimientos.objects.filter(etapa=estado)
                
            elif start != "Informe General" and estado != "no":     
                mantenimiento=mantenimientos.objects.filter(etapa=estado,fecha_inicio__range=[start,end])
                inicio=datetime.strptime(start,'%Y-%m-%d')
                fin=datetime.strptime(end,'%Y-%m-%d')
            usuario_actual=request.user.first_name +" "+ request.user.last_name
            
                                   
            context = {
                'start': inicio,
                'end': fin,
                'mantenimiento': mantenimiento,
                'usuario': usuario_actual,
                'comp': {'name': 'ALEMAN VERGARA PROYECTOS INMOBILIARIOS SAS', 'ruc': 'NIT: 900602576-9', 'address': 'DIRECCION: CLL 12 # 17 - 42 FONSECA, LA GUAJIRA','telefono':'TELEFONO: 7756344 – 3168347504'},
                'logo': '{}{}'.format(settings.STATIC_URL, 'img/Logo.jpg')
            }
            html = template.render(context)
            response = HttpResponse(content_type='application/pdf')
            #response['Content-Disposition'] = 'attachment; filename="report.pdf"'
            pisaStatus = pisa.CreatePDF(
                html, dest=response,
                link_callback=self.link_callback
            )
            return response
        except:
            pass
        return HttpResponseRedirect(reverse_lazy('erp:ventas'))


class FichaTecnicaPDFView(LoginRequiredMixin,ValidatePermissionRequiredMixin,View):
    permission_required=('Logistica y Compras','Marketing y Ventas')
    login_url='login'
    redirect_field_name='next'
    def link_callback(self, uri, rel):
        """
        Convert HTML URIs to absolute system paths so xhtml2pdf can access those
        resources
        """
        # use short variable names
        sUrl = settings.STATIC_URL  # Typically /static/
        sRoot = settings.STATIC_ROOT  # Typically /home/userX/project_static/
        mUrl = settings.MEDIA_URL  # Typically /static/media/
        mRoot = settings.MEDIA_ROOT  # Typically /home/userX/project_static/media/

        # convert URIs to absolute system paths
        if uri.startswith(mUrl):
            path = os.path.join(mRoot, uri.replace(mUrl, ""))
        elif uri.startswith(sUrl):
            path = os.path.join(sRoot, uri.replace(sUrl, ""))
        else:
            return uri  # handle absolute uri (ie: http://some.tld/foo.png)

        # make sure that file exists
        if not os.path.isfile(path):
            raise Exception(
                'media URI must start with %s or %s' % (sUrl, mUrl)
            )
        return path

    def get(self, request, *args, **kwargs):
        ubicacion=""
        imagen=""
        try:
            template = get_template('reportes/ficha_tecnica.html')
            id=self.kwargs['id']
            inmueble=inmuebles.objects.get(id=id)
            foto=inmueble.foto
            foto1=inmueble.foto1
            foto2=inmueble.foto2
            foto3=inmueble.foto3
            geo=inmueble.ubicacion
            if foto:
                imagen='{}{}'.format(settings.MEDIA_URL, foto)
            else:
                imagen='{}{}'.format(settings.STATIC_URL, "img/casa_defaul.jpg")
            
            if foto1:
                imagen1='{}{}'.format(settings.MEDIA_URL, foto1)
            else:
                imagen1='{}{}'.format(settings.STATIC_URL, "img/casa_defaul.jpg")
            
            if foto2:
                imagen2='{}{}'.format(settings.MEDIA_URL, foto2)
            else:
                imagen2='{}{}'.format(settings.STATIC_URL, "img/casa_defaul.jpg")
            
            if foto3:
                imagen3='{}{}'.format(settings.MEDIA_URL, foto3)
            else:
                imagen3='{}{}'.format(settings.STATIC_URL, "img/casa_defaul.jpg")


            if geo:
                ubicacion='{}{}'.format(settings.MEDIA_URL, geo)
            else:
                ubicacion='{}{}'.format(settings.STATIC_URL, "img/ubicacion.JPG")
                
            context = {
                #'novedad': novedad,
                #'visita': visita,
                #'presupuesto': presupuesto,
                'inmueble': inmueble,
                'cons':  id,
                'SI':  'SI',
               # 'detalle': "Casa en alquiler en la ciudad de Fonseca La Guajira"
                'ubicacion': ubicacion,
                'comp': {'name': 'ALEMAN VERGARA PROYECTOS INMOBILIARIOS SAS', 'ruc': 'NIT: 900602576-9', 'address': 'DIRECCION: CLL 12 # 17 - 42 FONSECA, LA GUAJIRA','telefono':'TELEFONO: +57(605)7756666 – 318-5499250'},
                'logo': '{}{}'.format(settings.STATIC_URL, 'img/Logo.jpg'),
                'foto': imagen,
                'foto1': imagen1,
                'foto2': imagen2,
                'foto3': imagen3,
                'precio_venta': '{:,.2f}'.format(inmueble.precio_venta).replace(",", "@").replace(".", ",").replace("@", "."),
                'precio_alquiler': '{:,.2f}'.format(inmueble.precio_alquiler).replace(",", "@").replace(".", ",").replace("@", "."), 
            }
            html = template.render(context)
            response = HttpResponse(content_type='application/pdf')
            #response['Content-Disposition'] = 'attachment; filename="report.pdf"'
            pisaStatus = pisa.CreatePDF(
                html, dest=response,
                link_callback=self.link_callback
            )
            return response
        except:
            pass
        return HttpResponseRedirect(reverse_lazy('erp:fichamodal'))

class ConstructorPDFView(LoginRequiredMixin,ValidatePermissionRequiredMixin,View):
    permission_required=('Ferreteria','Administrador')
    login_url='login'
    redirect_field_name='next'
    def link_callback(self, uri, rel):
        """
        Convert HTML URIs to absolute system paths so xhtml2pdf can access those
        resources
        """
        # use short variable names
        sUrl = settings.STATIC_URL  # Typically /static/
        sRoot = settings.STATIC_ROOT  # Typically /home/userX/project_static/
        mUrl = settings.MEDIA_URL  # Typically /static/media/
        mRoot = settings.MEDIA_ROOT  # Typically /home/userX/project_static/media/

        # convert URIs to absolute system paths
        if uri.startswith(mUrl):
            path = os.path.join(mRoot, uri.replace(mUrl, ""))
        elif uri.startswith(sUrl):
            path = os.path.join(sRoot, uri.replace(sUrl, ""))
        else:
            return uri  # handle absolute uri (ie: http://some.tld/foo.png)

        # make sure that file exists
        if not os.path.isfile(path):
            raise Exception(
                'media URI must start with %s or %s' % (sUrl, mUrl)
            )
        return path

    def get(self, request, *args, **kwargs):
           try:
            template = get_template('reportes/socio_constructor.html')
            id=self.kwargs['id']
            formato=clientes.objects.get(cedula=id)
            qr='{}{}'.format(settings.MEDIA_URL, str(id)+".png")
            compañia=compañias.objects.filter(cliente_id=id)
            banco=cuentas.objects.filter(cliente_id=id)
            if compañia and banco:
                cuenta=cuentas.objects.get(cliente_id=id)
                empresa=compañias.objects.get(cliente_id=id)
                context = {
                    'banco': cuenta,
                    'compañia': empresa,
                    'formato': formato,
                    'logo': '{}{}'.format(settings.STATIC_URL, 'img/logoferreteria.jpg'),
                    'qr': qr,
                        }
            elif banco:
                cuenta=cuentas.objects.get(cliente_id=id)
                context = {
                    'banco': cuenta,
                    'formato': formato,
                    'logo': '{}{}'.format(settings.STATIC_URL, 'img/logoferreteria.jpg'),
                    'qr': qr,
                        }
            elif compañia:
                empresa=compañias.objects.get(cliente_id=id)
                context = {
                    'compañia': empresa,
                    'formato': formato,
                    'logo': '{}{}'.format(settings.STATIC_URL, 'img/logoferreteria.jpg'),
                    'qr': qr,
                        }            
            else:
                context = {
                    'formato': formato,
                    'logo': '{}{}'.format(settings.STATIC_URL, 'img/logoferreteria.jpg'),
                    'qr': qr,
                        }


            html = template.render(context)
            response = HttpResponse(content_type='application/pdf')
            #response['Content-Disposition'] = 'attachment; filename="report.pdf"'
            pisaStatus = pisa.CreatePDF(
                html, dest=response,
                link_callback=self.link_callback
            )
            return response
           except:
              pass
           return HttpResponseRedirect(reverse_lazy('erp:ventas'))


class InformeinmueblesPDFView(LoginRequiredMixin,ValidatePermissionRequiredMixin,View):
    permission_required=('Logistica y Compras','Marketing y Ventas')
    login_url='login'
    redirect_field_name='next'
    def link_callback(self, uri, rel):
        """
        Convert HTML URIs to absolute system paths so xhtml2pdf can access those
        resources
        """
        # use short variable names
        sUrl = settings.STATIC_URL  # Typically /static/
        sRoot = settings.STATIC_ROOT  # Typically /home/userX/project_static/
        mUrl = settings.MEDIA_URL  # Typically /static/media/
        mRoot = settings.MEDIA_ROOT  # Typically /home/userX/project_static/media/

        # convert URIs to absolute system paths
        if uri.startswith(mUrl):
            path = os.path.join(mRoot, uri.replace(mUrl, ""))
        elif uri.startswith(sUrl):
            path = os.path.join(sRoot, uri.replace(sUrl, ""))
        else:
            return uri  # handle absolute uri (ie: http://some.tld/foo.png)

        # make sure that file exists
        if not os.path.isfile(path):
            raise Exception(
                'media URI must start with %s or %s' % (sUrl, mUrl)
            )
        return path
   
    def get(self, request, *args, **kwargs):
        
         try:
            template = get_template('reportes/listadoinmuebles.html')
            estatus=self.kwargs['estatus']
            contrato=self.kwargs['contrato']
            
            if contrato=="Informe General":
                inmueble=inmuebles.objects.all()
            elif contrato !="no" and estatus!="no":
                inmueble=inmuebles.objects.filter(estatus=estatus,tipo_negocio_id=int(contrato))
            elif contrato =="no" and estatus!="no":
                inmueble=inmuebles.objects.filter(estatus=estatus)
            elif contrato !="no" and estatus =="no":
                inmueble=inmuebles.objects.filter(tipo_negocio_id=int(contrato))
            usuario_actual=request.user.first_name +" "+ request.user.last_name
            
                                   
            context = {
                'inmueble': inmueble,
                'usuario': usuario_actual,
                'comp': {'name': 'ALEMAN VERGARA PROYECTOS INMOBILIARIOS SAS', 'ruc': 'NIT: 900602576-9', 'address': 'DIRECCION: CLL 12 # 17 - 42 FONSECA, LA GUAJIRA','telefono':'TELEFONO: 7756344 – 3168347504'},
                'logo': '{}{}'.format(settings.STATIC_URL, 'img/Logo.jpg')
            }
            html = template.render(context)
            response = HttpResponse(content_type='application/pdf')
            #response['Content-Disposition'] = 'attachment; filename="report.pdf"'
            pisaStatus = pisa.CreatePDF(
                html, dest=response,
                link_callback=self.link_callback
            )
            return response
         except:
             pass
         return HttpResponseRedirect(reverse_lazy('erp:ventas'))

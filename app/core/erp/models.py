from django.db import models
from django.utils import timezone
import uuid
from .choices import *
from django.forms import model_to_dict
from django.utils import timezone
from config.settings import MEDIA_URL, STATIC_URL
# Create your models here.
def directory_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = '{}.{}'.format(uuid.uuid4().hex[:8], ext)
    # return the whole path to the file
    return "{0}/{1}{2}".format(instance, filename)

class ubicacion(models.Model):
    id=models.AutoField(primary_key=True)
    ubicacion=models.CharField(max_length=200,verbose_name="Barrio")
    def toJSON(self):
        item = model_to_dict(self)
        return item    
    
    def barrio(self):
        return self.ubicacion
    
    def __str__(self):
        return self.barrio()


class tipos_inmuebles(models.Model):
    id=models.AutoField(primary_key=True)
    categorias=models.CharField(max_length=200,verbose_name="Tipo de Inmueble")
    def toJSON(self):
        item = model_to_dict(self)
        return item    
    
    def tipos(self):
        return self.categorias
    
    def __str__(self):
        return self.tipos()


class tipos_negocios(models.Model):
    id=models.AutoField(primary_key=True)
    tipo=models.CharField(max_length=200,verbose_name="Tipo de Negocio")
    def toJSON(self):
        item = model_to_dict(self)
        return item    
    
    def tipos(self):
        return self.tipo
    
    def __str__(self):
        return self.tipos()

class tipo_clentes(models.Model):
    id=models.AutoField(primary_key=True)
    tipo_clente=models.CharField(max_length=100,verbose_name="Tipo Clientes" ,null=True,blank=True)

    def toJSON(self):
        item = model_to_dict(self)
        return item    
    
    def tipoclient(self):
        return self.tipo_clente
    
    def __str__(self):
        return self.tipoclient()

class clientes(models.Model):
    tipo_persona=models.CharField(max_length=25,choices=tipo_persona,verbose_name="Tipo de Persona")
    cedula=models.BigIntegerField(primary_key=True,verbose_name="Cedula o Nit")
    ciudad_expedicion=models.CharField(max_length=100,verbose_name="Ciudad de Expedición" ,null=True,blank=True)
    nombre=models.CharField(max_length=100,verbose_name="Nombre")
    genero=models.CharField(max_length=25,verbose_name="Genero",choices=genero,null=True,blank=True)
    correo_electronico=models.CharField(max_length=100,verbose_name="Correo Electronico" ,null=True,blank=True)
    telefono=models.CharField(max_length=100,verbose_name="Telefono")
    direccion=models.CharField(max_length=100,verbose_name="Direccion" ,null=True,blank=True)
    tipo_cliente=models.ForeignKey(tipo_clentes,null=True,blank=True,verbose_name="Tipo de Cliente",on_delete=models.CASCADE)
    ciudad_domicilo=models.CharField(max_length=100,verbose_name="Ciudad de Domicilio" ,null=True,blank=True)
    numero_anual=models.IntegerField(verbose_name="Numero",null=True,blank=True)
    numero_mensual=models.IntegerField(verbose_name="Numero",null=True,blank=True)
    fecha=models.DateField(default=timezone.now,verbose_name="Fecha",null=True,blank=True)
    consecutivo=models.CharField(max_length=100,verbose_name="Consecutivo",null=True,blank=True)
    codigo=models.CharField(max_length=100,verbose_name="Codigo" ,null=True,blank=True)
    referencia=models.CharField(max_length=100,verbose_name="Referencia" ,null=True,blank=True)
    def natural_key(selft):
        return(selft.nombre)

    def toJSON(self):
        item = model_to_dict(self)
        return item
    def nombre_cliente(self):
        return self.nombre
    
    def __str__(self):
        return self.nombre_cliente()
    

class puntaje(models.Model):
    id=models.AutoField(primary_key=True)
    numero_factura=models.CharField(max_length=100,verbose_name="Factura",null=True,blank=True)
    detalle_canje=models.CharField(max_length=100,verbose_name="Detalle",null=True,blank=True)
    fecha=models.DateField(default=timezone.now,verbose_name="Fecha",null=True,blank=True)
    vendedor=models.CharField(max_length=100,verbose_name="vendedor",null=True,blank=True)
    total_factura=models.DecimalField(max_digits = 15, decimal_places = 2,verbose_name="Total",default="0.00",null=True,blank=True)
    comision=models.DecimalField(max_digits = 15, decimal_places = 2,verbose_name="Comision",default="0.00",null=True,blank=True)
    puntaje_total=models.BigIntegerField(verbose_name="Puntaje",null=True,blank=True)
    canjeados=models.BigIntegerField(verbose_name="Puntaje A Canjear",null=True,blank=True,default=0)
    operacion=models.CharField(max_length=100,verbose_name="Factura",null=True,blank=True)
    cliente=models.ForeignKey(clientes,verbose_name="Cliente",null=True,blank=True,on_delete=models.CASCADE)
    nivel=models.BigIntegerField(verbose_name="Nivel",null=True,blank=True)
    class Meta:
        ordering = ['id']
        get_latest_by = 'id'
        
    def __str__(self):
        return self.name, puntaje.objects.filter(state=True)

    def toJSON(self):
        item = model_to_dict(self)
        return item


class elementos(models.Model):
    id=models.AutoField(primary_key=True)
    elemento=models.CharField(max_length=100,verbose_name="elemento")
   
    def __str__(self):
        return self.elemento, elementos.objects.filter(state=True)

    def toJSON(self):
        item = model_to_dict(self)
        return item


class inmuebles(models.Model):
    id=models.AutoField(primary_key=True)
    matricula=models.CharField(max_length=25,verbose_name="Matricula Inmobiliaria")
    clientes=models.ForeignKey(clientes,verbose_name="propietario",null=True,blank=True,on_delete=models.CASCADE)
    ubucacion=models.ForeignKey(ubicacion,verbose_name="Barrio",on_delete=models.CASCADE)
    direccion=models.CharField(max_length=300,verbose_name="Direccion")
    ciudad=models.CharField(max_length=300,verbose_name="Ciudad")
    fecha=models.DateField(default=timezone.now, verbose_name="Fecha")
    categorias=models.ForeignKey(tipos_inmuebles,max_length=100,verbose_name="Tipo de Inmueble",on_delete=models.CASCADE)
    tipo_negocio=models.ForeignKey(tipos_negocios,max_length=100,verbose_name="Servicio",on_delete=models.CASCADE)
    alcobas=models.CharField(max_length=25,verbose_name="Alcobas",choices=alcobas)
    baños=models.CharField(max_length=25,verbose_name="Baños" ,choices=baños)
    garajes=models.CharField(max_length=25,verbose_name="Garaje",choices=garajes)
    piso=models.CharField(max_length=25,verbose_name="Piso",choices=piso)
    sala=models.CharField(max_length=25,verbose_name="Sala",choices=afirmacion)
    comedor=models.CharField(max_length=25,verbose_name="Comedor",choices=afirmacion)
    cocina=models.CharField(max_length=25,verbose_name="Cocina",choices=afirmacion)
    patio=models.CharField(max_length=25,verbose_name="Patio",choices=afirmacion)
    estufa=models.CharField(max_length=25,verbose_name="Estufa",choices=afirmacion,null=True,blank=True)
    campana=models.CharField(max_length=25,verbose_name="Campana",choices=afirmacion,null=True,blank=True)
    gabinete_superior=models.CharField(max_length=25,verbose_name="Gabinete Inferior",choices=afirmacion,null=True,blank=True)
    gabinete_inferior=models.CharField(max_length=25,verbose_name="Gabinete Superior",choices=afirmacion,null=True,blank=True)
    aire_acondicionado=models.CharField(max_length=25,verbose_name="Aire Acondicionado",choices=afirmacion,null=True,blank=True)
    contador_gas=models.CharField(max_length=25,verbose_name="Contador de Gas",choices=afirmacion,null=True,blank=True)
    contador_energia=models.CharField(max_length=25,verbose_name="Contador de energia",choices=afirmacion,null=True,blank=True)
    contador_agua=models.CharField(max_length=25,verbose_name="Contador de Agua",choices=afirmacion,null=True,blank=True)
    cortineras=models.CharField(max_length=25,verbose_name="Cortineras",choices=afirmacion,null=True,blank=True)
    instalacion_gas=models.CharField(max_length=25,verbose_name="Instalacion de Gas",choices=afirmacion,null=True,blank=True)
    filtro_agua=models.CharField(max_length=25,verbose_name="Filtro de Agua",choices=afirmacion,null=True,blank=True)
    balcon=models.CharField(max_length=25,verbose_name="Balcon",choices=afirmacion,null=True,blank=True)
    muebles_baño=models.CharField(max_length=25,verbose_name="Muebles en el Baño",choices=afirmacion,null=True,blank=True)
    amoblado=models.CharField(max_length=25,verbose_name="Amoblada",choices=afirmacion,null=True,blank=True)
    estrato=models.CharField(max_length=25,verbose_name="Estrato",choices=estrato)
    area_privada=models.DecimalField(max_digits=12,decimal_places=2,default='0.0',verbose_name='Area Privada')
    area_construida=models.DecimalField(max_digits=12,decimal_places=2,default='0.0',verbose_name='Area Construida')
    area_total=models.DecimalField(max_digits=12,decimal_places=2,default='0.0',verbose_name='Area total')
    estado=models.CharField(max_length=100,verbose_name="Estado" ,choices=estados)
    precio_venta=models.DecimalField(max_digits = 12, decimal_places = 2,default='0.00',verbose_name="Precio de Venta",null=True,blank=True)
    precio_alquiler=models.DecimalField(max_digits = 12, decimal_places = 2,default='0.00',verbose_name="Precio de Alquiler",null=True,blank=True)
    precio_administracion=models.DecimalField(max_digits = 12, decimal_places = 2,default='0.00',verbose_name="Valor Administracion",null=True,blank=True)
    fecha_ultimo=models.DateField(default=timezone.now,verbose_name="Ultimo mantenimiento",null=True,blank=True)
    fecha_mantenimiento=models.DateField(default=timezone.now,verbose_name="Fecha Proximo Mantenimiento",null=True,blank=True)
    observacion=models.TextField(max_length=1000,verbose_name="Detalles" ,null=True,blank=True)
    linderos=models.TextField(max_length=1000,verbose_name="Linderos" ,null=True,blank=True)
    detalle_mantenimiento=models.TextField(max_length=1000,verbose_name="Mantenimiento Realizado" ,null=True,blank=True)
    foto=models.ImageField(default=None,upload_to='inmuebles/%Y/%m/%d',verbose_name='Imagen 1',null=True,blank=True)
    foto1=models.ImageField(default=None,upload_to='inmuebles/%Y/%m/%d',verbose_name='Imagen 2',null=True,blank=True)
    foto2=models.ImageField(default=None,upload_to='inmuebles/%Y/%m/%d',verbose_name='Imagen 3',null=True,blank=True)
    foto3=models.ImageField(default=None,upload_to='inmuebles/%Y/%m/%d',verbose_name='Imagen 4',null=True,blank=True)
    ubicacion=models.ImageField(default=None,upload_to='inmuebles/%Y/%m/%d',verbose_name='Ubicacion',null=True,blank=True)
    usuario=models.CharField(max_length=50,verbose_name="Usuario",null=True,blank=True)
    estatus=models.CharField(max_length=25,verbose_name="Estatus",choices=estatus,null=True,blank=True)
    def natural_key(selft):
        return(selft.direccion)
    
    def toJSON(self):
        item = model_to_dict(self)
        return item

    
    def direccion_inmuebles(self):
        return self.direccion
    
    def __str__(self):
        return self.direccion_inmuebles()
    
    
class espacios(models.Model):
    id=models.AutoField(primary_key=True)
    espacio=models.CharField(max_length=100,verbose_name="Espacio")
   
    def __str__(self):
        return self.espacio, espacios.objects.filter(state=True)

    def toJSON(self):
        item = model_to_dict(self)
        return item


class novedades(models.Model):
    id=models.AutoField(primary_key=True,verbose_name="codigo")
    inmueble_novedad=models.ForeignKey(inmuebles,max_length=100,verbose_name="Inmueble",on_delete=models.CASCADE)
    usuario=models.CharField(max_length=100,verbose_name="Usuario" ,null=True,blank=True)
    usuario_novedad=models.CharField(max_length=100,verbose_name="Reportado Por" ,null=True,blank=True)
    novedad=models.TextField(max_length=500,verbose_name="Novedad" ,null=True,blank=True)
    estado_novedad=models.CharField(max_length=100,verbose_name="Estado de la Novedad")
    fecha_novedad=models.DateField(default=timezone.now,verbose_name="fecha Novedad",null=True,blank=True)
    
    class Meta:
        ordering = ["-fecha_novedad"]
    
    
    def toJSON(self):
        item = model_to_dict(self)
        return item
    
class mantenimientos(models.Model):
    id=models.AutoField(primary_key=True,)
    inmueble=models.CharField(max_length=100,verbose_name="inmueble" ,null=True,blank=True)
    descripcion_actividad=models.TextField(max_length=500,verbose_name="Descripcion Actividad" ,null=True,blank=True)
    usuario=models.CharField(max_length=100,verbose_name="Usuario" ,null=True,blank=True)
    fecha_inicio=models.DateField(default=timezone.now,verbose_name="Fecha de Inicio",null=True,blank=True)
    fecha_terminacion=models.DateField(verbose_name="Fecha de Terminacion",null=True,blank=True)
    tipo_mantenimiento=models.CharField(max_length=100,verbose_name="Tipo de Mantenimiento",choices=tipo_mantenimiento)
    estado_mantenimiento=models.CharField(max_length=100,verbose_name="Estado del Mantenimiento",choices=estados_mantenimiento)
    consecutivo_novedad=models.CharField(max_length=100,verbose_name="consecutivo" ,null=True,blank=True)
    usuario_novedad=models.CharField(max_length=100,verbose_name="Reportado Por" ,null=True,blank=True)
    etapa=models.CharField(max_length=100,verbose_name="Etapa" ,null=True,blank=True)
    class Meta:
        get_latest_by = 'id'
        ordering = ["-id"]
    
    def toJSON(self):
        item = model_to_dict(self)
        return item

class Procedimiento(models.Model):
    id=models.AutoField(primary_key=True)
    procedimientos=models.CharField(max_length=100,verbose_name="Procedimiento")

    def toJSON(self):
        item = model_to_dict(self)
        return item
    def procedimientos_novedad(self):
        return self.procedimientos
    
    def __str__(self):
        return self.procedimientos_novedad()



class detalles_mantenimiento(models.Model):
    id=models.AutoField(primary_key=True)
    descripcion_actividad=models.TextField(max_length=500,verbose_name="Descripcion Actividad" ,null=True,blank=True)
    id_mantenimiento=models.BigIntegerField(verbose_name="Mantenimiento",null=True,blank=True)
    usuario=models.CharField(max_length=100,verbose_name="Usuario" ,null=True,blank=True)
    fecha=models.DateField(default=timezone.now,verbose_name="Fecha",null=True,blank=True)
    contratista=models.CharField(max_length=100,verbose_name="Contratista" ,null=True,blank=True)
    inmueble=models.CharField(max_length=100,verbose_name="inmueble" ,null=True,blank=True)
    usuario_reporte=models.CharField(max_length=100,verbose_name="Reportado Por" ,null=True,blank=True)
    recibido_por=models.CharField(max_length=100,verbose_name="Recibido Por" ,null=True,blank=True)
    fecha_etapa=models.DateField(default=timezone.now,verbose_name="Fecha",null=True,blank=True)
    etapa=models.CharField(max_length=100,verbose_name="Etapa" ,null=True,blank=True)
    estado=models.CharField(max_length=100,verbose_name="Estado" ,null=True,blank=True,choices=estado)
    hora=models.TimeField(auto_now=True, auto_now_add=False ,null=True,blank=True)
    procedimientos=models.ForeignKey(Procedimiento,max_length=100,verbose_name="Procedimiento",null=True,blank=True,on_delete=models.CASCADE)
    autoricacion=models.CharField(max_length=100,verbose_name="Autorizado" ,null=True,blank=True,choices=afirmacion)
    preventivo=models.CharField(max_length=100,verbose_name="Preventivo",null=True,blank=True,choices=afirmacion)
    responsable=models.CharField(max_length=100,verbose_name="Responsable",null=True,blank=True,choices=responsable)
    fecha_ultimo=models.DateField(verbose_name=" Fecha Mantenimiento",null=True,blank=True)
    fecha_mantenimiento=models.DateField(verbose_name="Proximo Mantenimiento",null=True,blank=True)
    finalizacion=models.CharField(max_length=100,verbose_name="Finalizado" ,null=True,blank=True,choices=afirmacion)
    class Meta:
        ordering = ['id']
        get_latest_by = 'id'
        
        
    def toJSON(self):
        item = model_to_dict(self)
        return item
class compañias(models.Model):
    id=models.AutoField(primary_key=True)
    nit=models.CharField(max_length=100,verbose_name="NIT")
    razon_social=models.CharField(max_length=100,verbose_name="Razon Social")
    telefono=models.CharField(max_length=100,verbose_name="Telefono",null=True,blank=True)
    direccion=models.CharField(max_length=100,verbose_name="Direccion",null=True,blank=True)
    ciudad=models.CharField(max_length=100,verbose_name="Ciudad",null=True,blank=True)
    correo_electronico=models.CharField(max_length=100,verbose_name="correo Electronico",null=True,blank=True)
    cliente=models.ForeignKey(clientes,max_length=100,verbose_name="Cliente",on_delete=models.CASCADE,null=True,blank=True)
    def toJSON(self):
        item = model_to_dict(self)
        return item
    def nombre_compañia(self):
        return self.razon_social
    
    def __str__(self):
        return self.nombre_compañia()


class codeudores(models.Model):
    tipo_persona=models.CharField(max_length=25,choices=tipo_persona,verbose_name="Tipo de Persona")
    cedula=models.BigIntegerField(primary_key=True,verbose_name="Cedula")
    ciudad_expedicion=models.CharField(max_length=100,verbose_name="Ciudad de Expedición" ,null=True,blank=True)
    nombre=models.CharField(max_length=100,verbose_name="Nombre")
    genero=models.CharField(max_length=25,verbose_name="Genero",choices=genero,null=True,blank=True)
    correo_electronico=models.CharField(max_length=100,verbose_name="Correo Electronico" ,null=True,blank=True)
    telefono=models.CharField(max_length=100,verbose_name="Telefono")
    direccion=models.CharField(max_length=100,verbose_name="Direccion" ,null=True,blank=True)
    ciudad_domicilo=models.CharField(max_length=100,verbose_name="Ciudad de Domicilio" ,null=True,blank=True)
    def toJSON(self):
        item = model_to_dict(self)
        return item
    def nombre_cliente(self):
        return self.nombre
    
    def __str__(self):
        return self.nombre_cliente()
    
    
class representantes(models.Model):
    tipo_persona=models.CharField(max_length=25,choices=tipo_persona,verbose_name="Tipo de Persona")
    cedula=models.BigIntegerField(primary_key=True,verbose_name="Cedula")
    ciudad_expedicion=models.CharField(max_length=100,verbose_name="Ciudad de Expedición" ,null=True,blank=True)
    nombre=models.CharField(max_length=100,verbose_name="Nombre")
    genero=models.CharField(max_length=25,verbose_name="Genero",choices=genero,null=True,blank=True)
    correo_electronico=models.CharField(max_length=100,verbose_name="Correo Electronico" ,null=True,blank=True)
    telefono=models.CharField(max_length=100,verbose_name="Telefono")
    direccion=models.CharField(max_length=100,verbose_name="Direccion" ,null=True,blank=True)
    ciudad_domicilo=models.CharField(max_length=100,verbose_name="Ciudad de Domicilio" ,null=True,blank=True)
    def toJSON(self):
        item = model_to_dict(self)
        return item
    def nombre_cliente(self):
        return self.nombre
    
    def __str__(self):
        return self.nombre_cliente()


class contratos(models.Model):
    id=models.AutoField(primary_key=True)
    consecutivo=models.CharField(max_length=100,verbose_name="Consecutivo",null=True,blank=True)
    codigo=models.CharField(max_length=100,verbose_name="Codigo" ,null=True,blank=True)
    referencia=models.CharField(max_length=100,verbose_name="Referencia" ,null=True,blank=True)
    tipo_contrato=models.CharField(max_length=100,verbose_name="Contratos",choices=contrato ,null=True,blank=True)
    representante_legal=models.ForeignKey(representantes,null=True,blank=True, max_length=100,verbose_name="Representante Legal",on_delete=models.CASCADE)
    inmueble=models.ForeignKey(inmuebles,max_length=100,verbose_name="Inmueble",on_delete=models.CASCADE,null=True,blank=True)
    precio=models.DecimalField(max_digits = 12, decimal_places = 2,default='0.00',verbose_name="Precio",null=True,blank=True)
    duracion_contrato=models.CharField(max_length=50,verbose_name="Duracion del Contrato",null=True,blank=True)
    categorias=models.ForeignKey(tipos_inmuebles,max_length=100,verbose_name="Tipo de Inmueble",on_delete=models.CASCADE,null=True,blank=True)
    fecha=models.DateField(default=timezone.now,verbose_name="Fecha",null=True,blank=True)
    arrendatario=models.ForeignKey(clientes,max_length=100,verbose_name="Cliente",on_delete=models.CASCADE,null=True,blank=True)
    codeudor=models.ForeignKey(codeudores,max_length=100,verbose_name="Codeudor",on_delete=models.CASCADE,null=True,blank=True)
    numero_anual=models.IntegerField(verbose_name="Numero",null=True,blank=True)
    numero_mensual=models.IntegerField(verbose_name="Numero",null=True,blank=True)
    compañia=models.ForeignKey(compañias,max_length=100,verbose_name="Compañia",on_delete=models.CASCADE,null=True,blank=True)
    class Meta:
        ordering = ['id']
        get_latest_by = 'id'
        
        
    def toJSON(self):
        item = model_to_dict(self)
        return item

class cuerpoContrato(models.Model):
    id=models.AutoField(primary_key=True)
    cuerpo=models.TextField(max_length=40000,null=True,blank=True)
    tipo_contrato=models.CharField(max_length=100,verbose_name="Tipo" ,null=True,blank=True)


class cuentas(models.Model):
    id=models.AutoField(primary_key=True)
    nombre_banco=models.CharField(max_length=100,null=True,blank=True,verbose_name="Banco")
    tipo_cuenta=models.CharField(max_length=100,verbose_name="Tipo De cuenta",null=True,blank=True,choices=tipo_cuenta)
    numero_cuenta=models.BigIntegerField(verbose_name="Numero de Cuenta",null=True,blank=True)
    cliente=models.ForeignKey(clientes,max_length=100,verbose_name="Titular de la cuenta",on_delete=models.CASCADE,null=True,blank=True)
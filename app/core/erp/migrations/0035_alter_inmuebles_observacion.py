# Generated by Django 4.1 on 2023-04-23 04:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('erp', '0034_rename_foto1_inmuebles_foto'),
    ]

    operations = [
        migrations.AlterField(
            model_name='inmuebles',
            name='observacion',
            field=models.TextField(blank=True, max_length=1000, null=True, verbose_name='Detalles'),
        ),
    ]

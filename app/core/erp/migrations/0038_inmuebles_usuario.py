# Generated by Django 4.1 on 2023-04-23 19:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('erp', '0037_inmuebles_aire_acondicionado_inmuebles_balcon_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='inmuebles',
            name='usuario',
            field=models.TextField(blank=True, max_length=50, null=True, verbose_name='Usuario'),
        ),
    ]

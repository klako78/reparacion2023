# Generated by Django 4.1 on 2023-04-16 21:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('erp', '0027_alter_puntaje_canjeados'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='puntaje',
            name='fecha_canje',
        ),
    ]

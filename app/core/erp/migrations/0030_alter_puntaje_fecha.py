# Generated by Django 4.1 on 2023-04-17 20:34

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('erp', '0029_puntaje_operacion'),
    ]

    operations = [
        migrations.AlterField(
            model_name='puntaje',
            name='fecha',
            field=models.DateField(blank=True, default=django.utils.timezone.now, null=True, verbose_name='Fecha'),
        ),
    ]

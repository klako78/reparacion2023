# Generated by Django 4.1 on 2023-07-31 17:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('erp', '0069_compañias_rename_numero_contratos_numero_anual_and_more'),
    ]

    operations = [
       
        migrations.AlterField(
            model_name='compañias',
            name='razon_social',
            field=models.CharField(max_length=100, verbose_name='Razon Social'),
        ),
    ]

# Generated by Django 4.1 on 2023-04-16 19:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('erp', '0026_puntaje_detalle_canje_puntaje_fecha_canje'),
    ]

    operations = [
        migrations.AlterField(
            model_name='puntaje',
            name='canjeados',
            field=models.BigIntegerField(blank=True, default=0, null=True, verbose_name='Puntaje A Canjear'),
        ),
    ]

# Generated by Django 4.1 on 2023-04-14 13:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('erp', '0024_alter_detalles_mantenimiento_fecha_mantenimiento_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='puntaje',
            name='fecha',
            field=models.DateField(blank=True, null=True, verbose_name='Fecha'),
        ),
    ]

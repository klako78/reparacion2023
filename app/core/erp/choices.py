
tipo_inmueble=(
           ('casa', 'Casa'),
           ('apartamento', 'Apartamento'),
           ('aparta estudio', 'Aparta Estudio'),
           ('habitacion', 'Habitacion'),
           ('oficina', 'Oficina'),
           ('local', 'Local'),
           ('bodega', 'Bodega'),
        )
inmueble=(
           ('vivienda', 'Vivienda'),
           ('local_comercial', 'Local comercial'),
           
        )

tip_cliente=(
           ('propietario', 'Propietario'),
           ('arrendatario', 'Arrendatario'),
           ('codeudor', 'Codeudor'),
           
        )
estado=(
           ('Ejecutandose', 'Ejecutandose'),
           ('Radicada', 'Radicada'),
           ('Ejecutada', 'Ejecutada'),
           ('No Ejecutada', 'No Ejecutada'),
           
        )
tipo_mantenimiento=(
           ('Preventivo', 'Preventivo'),
           ('Correctivo', 'Correctivo'),
           
           
        )

tipo_persona=(
           ('Natural', 'Natural'),
           ('Juridica', 'Juridica'),
                   
        )

estados=(
           ('Usado', 'Usado'),
           ('Nuevo', 'Nuevo'),
           ('Proyecto','Proyecto'),
           ('En Construccion','En Construccion'),
                   
        )        


alcobas=(
           ('0', '0'),
           ('1', '1'),
           ('2', '2'),
           ('3', '3'),
           ('4', '4'),
           ('5', '5'),
           ('6', '6'),
           ('7', '7'),
           ('8', '8'),
           ('9', '9'),
           ('10', '10'),
           ('11', '11'),
           ('12', '12'),
           ('13', '13'),
           ('14', '14'),
           ('15', '15'),
           ('>15', '>15'),         
        )        

baños=(
           ('0', '0'),
           ('1', '1'),
           ('2', '2'),
           ('3', '3'),
           ('4', '4'),
           ('5', '5'),
           ('6', '6'),
           ('7', '7'),
           ('8', '8'),
           ('9', '9'),
           ('10', '10'),
           ('11', '11'),
           ('12', '12'),
           ('13', '13'),
           ('14', '14'),
           ('15', '15'),
           ('>15', '>15'),         
        )                

piso=(
           ('0', '0'),
           ('1', '1'),
           ('2', '2'),
           ('3', '3'),
           ('4', '4'),
           ('5', '5'),
           ('6', '6'),
           ('7', '7'),
           ('8', '8'),
           ('9', '9'),
           ('10', '10'),
           ('11', '11'),
           ('12', '12'),
           ('13', '13'),
           ('14', '14'),
           ('15', '15'),
           ('>15', '>15'),         
        )    

garajes=(
           ('0 Vehiculo', '0 Vehiculo'),
           ('1 Vehiculo', '1 vehiculo'),
           ('2 Vehiculos', '2 Vehiculos'),
           ('3 Vehiculos', '3 Vehiculos'),
           ('4 Vehiculos', '4 Vehiculos'),
           ('5 Vehiculos', '5 Vehiculos'),
           ('6 Vehiculos', '6 Vehiculos'),
           ('7 Vehiculos', '7 Vehiculos'),
           ('8 Vehiculos', '8 Vehiculos'),
           ('9 Vehiculos', '9 Vehiculos'),
           ('10 Vehiculos', '10 Vehiculos'),
           ('11 Vehiculos', '11 Vehiculos'),
           ('12 Vehiculos', '12 Vehiculos'),
           ('13 Vehiculos', '13 Vehiculos'),
           ('14 Vehiculos', '14 Vehiculos'),
           ('15 Vehiculos', '15 Vehiculos'),
           ('>15 Vehiculos', '>15 Vehiculos'),        
        )                        

estrato=(
           ('N/D', 'N/D'),
           ('1', '1'),
           ('2', '2'),
           ('3', '3'),
           ('4', '4'),
           ('5', '5'),
           ('6', '6'),
           ('Rural', 'Rural'),
           ('Comercial', 'Comercial'),
)

estados_mantenimiento=(
           ('EN EJECUCION', 'EN EJECUCION'),
           ('EJECUTADO', 'EJECUTADO'),
           ('NO EJECUTADO','NO EJECUTADO'),
          
                   
        )

etapas=(
           ('Novedad', 'Novedad'),
           ('Inspección', 'Inspección'),
           ('Presupuesto','Presupuesto'),
           ('Ejecucion','Ejecucion'),
           ('Entrega','Entrega'),
                   
        ) 

estado=(
           ('EN EJECUCION', 'EN EJECUCION'),
           ('EJECUTADO', 'EJECUTADO'),
           ('NO EJECUTADO','NO EJECUTADO'), )  

afirmacion=(
           ('SI', 'SI'),
           ('NO','NO'), ) 

responsable=(
           ('Propietario', 'Propietario'),
           ('Arrendatario','Arrendatario'), )                        

contrato=(
        ('Administracion', 'Administracion'),
        ('Corretaje', 'Corretaje'),
        ('Arrendamiento', 'Arrendamiento'),
)

genero=(
        ('Femenino', 'Femenino'),
        ('Masculino', 'Masculino'),
        
)

tipo_cuenta=(
        ('Ahorro', 'Ahorro'),
        ('Corriente', 'Corriente'),
        
)

estatus=(
        ('Disponible', 'Disponible'),
        ('No Disponible', 'No Disponible'),
        
)
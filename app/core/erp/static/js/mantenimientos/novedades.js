$(function(){
  var table=$("#data").DataTable({
    responsive: true,
    autoWidth: false,
    destroy: true,
    deferRender: true,
    order: [[3, 'desc']],
    ajax: {
      url: window.location.pathname,
      type: "POST",
      data: {
        action: "searchdata",
        
      },
      dataSrc: "",
    },
    columns: [
      { data: "id" },
      { data: "novedad" },
      { data: "estado_novedad" },
      { data: "fecha_novedad" },
      { data: "usuario" },
      {data:  "inmueble_novedad"},
      {data:  "usuario_novedad"},
      {"defaultContent": '<button type="button" class="btn btn-primary btn-success btn-flat btn" id="btn" name="btn" class="btn"><i class="fas fa-plus"></i> Nuevo</button>'},          
    ],
      
    
    columnDefs: [
      {
        targets: 3,
           
            render: DataTable.render.datetime('DD/MM/YYYY'),           
      },
      {
        targets:  5,
        
        visible: false,
    }
      
    ],
    
    initComplete: function (settings, json) {
    
    },
  
  });
  
 optener_novedad('#data tbody',table) 




});
var optener_novedad=function(tbody,table){
  $(tbody).on("click","button.btn",function(){
    var data=table.row($(this).parents("tr")).data();
     $('input[name="cedula"]').val(data.inmueble_novedad);
     $('input[name="action"]').val('add');
     $('input[name="novedad"]').val(data.id);
     $('input[name="descripcion_actividad"]').val(data.novedad);
     $('input[name="usuario_novedad"]').val(data.usuario_novedad);
     $('#mantenimientoModal').modal('show');
  });

   
}


    


 

$(function(){
  var table=$("#data").DataTable({
    responsive: true,
    autoWidth: false,
    destroy: true,
    deferRender: true,
    order: [[5, 'desc']],
    ajax: {
      url: window.location.pathname,
      type: "POST",
      data: {
        action: "searchdata",
        
      },
      dataSrc: "",
    },
    columns: [
      { data: "id" },
      { data: "descripcion_actividad" },
      { data: "usuario" },
      { data: "inmueble"},
      { data: "estado_mantenimiento" },
      { data: "fecha_inicio" },
      { data: "fecha_terminacion" },
      { data: "etapa"},
     
      {"defaultContent": '<button type="button" class="btndetalle btn-primary  btn-xm btn" id="btndetalle" name="btndetalle"><i class="fas fa-plus"></i></button> <button type="button" class="btnver btn-warning  btn-xm btn" id="btnver" name="btnver"><i class="fa fa-print"></i></button>'
      },        
      
    ],
    columnDefs: [
      {
        targets: 4,
           
            render: DataTable.render.datetime('DD/MM/YYYY'),
            

                   
      },
      {
        targets: 5,
           
            render: DataTable.render.datetime('DD/MM/YYYY'),
            

                   
      },
      {
        targets: 3,
           
          visible: false, 
            

                   
      },
      {
        targets: 0,
           
          visible: false, 
            

                   
      },
    ],
    
    initComplete: function (settings, json) {
    
    },
  
  });
  
 optener_novedad('#data tbody',table) 
 $.getScript("modals.js", function(){
  document.write(boton());
 });
});
    
optener_novedad=function(tbody,table){
  $(tbody).on("click","button.btndetalle",function(){
    var data=table.row($(this).parents("tr")).data();
    $('input[name="id_inmueble"]').val(data.inmueble);
    $('input[name="mantenimiento"]').val(data.id);
     $('input[name="id_novedad"]').val(data.consecutivo_novedad);
     $('#mantenimientoModal').modal('show');
      
     
  });
  $(tbody).on("click","button.btnver",function(){
    var data=table.row($(this).parents("tr")).data();
    location.assign('/erp/detalle/reportePDF/'+data.id); 
   
  }); 
   
}


  

   

 

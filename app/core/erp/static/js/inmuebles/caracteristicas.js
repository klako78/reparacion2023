$(function(){
  var table=$("#data").DataTable({
    responsive: true,
    autoWidth: false,
    destroy: true,
    deferRender: true,
    ajax: {
      url: window.location.pathname,
      type: "POST",
      data: {
        action: "searchall",
        
      },
      dataSrc: "",
      
    },
    columns: [
      { data: "matricula" },
      { data: "clientes" },
      { data: "ubucacion" },
      { data: "direccion" },
      { data: "precio_alquiler" },
      { data: "categorias" },
      {"defaultContent": '<button type="button" class="btnver btn-warning  btn-xm btn" id="btnver" name="btnver"><i class="fa fa-print"></i></button> <button type="button" class="btnedit btn-info  btn-xm btn" id="btnedit" name="btnedit"><i class="fa fa-edit"></i></button> '
      },    
    ],
    columnDefs: [
      
      {
        targets: 4,
        render: DataTable.render.number( '.', ',', 2,'$ ', ' ' ),
      },
    ],
    
    initComplete: function (settings, json) {
    
    },
  
  });
  
 optener_novedad('#data tbody',table) 




});

optener_novedad=function(tbody,table){
  
  $(tbody).on("click","button.btnver",function(){
    var data=table.row($(this).parents("tr")).data();
    location.assign('/erp/fichaPDF/'+data.id); 
   
  }); 

  $(tbody).on("click","button.btnedit",function(){
    var data=table.row($(this).parents("tr")).data();
    location.assign('/erp/inmuebleedit/'+data.id);
    
  }); 
   
}
    



  

   

 

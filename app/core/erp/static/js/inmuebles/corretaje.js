$(function(){
  var table=$("#data").DataTable({
    responsive: true,
    autoWidth: false,
    destroy: true,
    deferRender: true,
    ajax: {
      url: window.location.pathname,
      type: "POST",
      data: {
        action: "searchall",
        
      },
      dataSrc: "",
      
    },
    columns: [
      { data: "fecha" },
      { data: "codigo" },
      { data: "referencia" },
      { data: "consecutivo" },
      { data: "inmueble" },
      { data: "categorias" },
      { data: "arrendatario" },
      { data: "precio" },
      {"defaultContent": '<button type="button" class="btnver btn-warning  btn-xm btn" id="btnver" name="btnver"><i class="fa fa-print"></i></button>'
      }, 
          
    ],
    columnDefs: [
      {
      
      targets: 0,
           
            render: DataTable.render.datetime('DD/MM/YYYY'),
          },
          {
            targets: 7,
            render: DataTable.render.number( '.', ',', 2,'$ ', ' ' ),
          },    
      
      
    ],
    
    initComplete: function (settings, json) {
    
    },
  
  });
  
 optener_novedad('#data tbody',table) 




});

optener_novedad=function(tbody,table){
  
  $(tbody).on("click","button.btnver",function(){
    var data=table.row($(this).parents("tr")).data();
    location.assign('/erp/corretajePDF/'+data.id); 
   
  }); 
   
}
    



  

   

 

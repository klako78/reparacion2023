$("#data").DataTable({
  responsive: true,
  autoWidth: false,
  destroy: true,
  deferRender: true,
  order: [[3, 'desc']],
  ajax: {
    url: window.location.pathname,
    type: "POST",
    data: {
      action: "searchdata",
    
    },
    dataSrc: "",
  },
  columns: [
    { data: "fecha" },
    { data: "numero_factura" },
    { data: "total_factura" },
    { data: "vendedor" },
    { data: "puntaje_total" },
    
  ],
  columnDefs: [
    {
      targets: 0,
      render: DataTable.render.datetime('DD/MM/YYYY'),

                 
    },
    {
      targets: 2,
      render: DataTable.render.number( '.', ',', 2,'$ ', ' ' ),
     
                 
    },
    
  ],
  
  initComplete: function (settings, json) {
   var suma = $('#data').DataTable().column(4).data().sum();
    $('#total').html(suma);
   
  },

});


$("#data1").DataTable({
  responsive: true,
  autoWidth: false,
  destroy: true,
  deferRender: true,
  order: [[3, 'desc']],
  ajax: {
    url: window.location.pathname,
    type: "POST",
    data: {
      action: "searchdata1",
    
    },
    dataSrc: "",
  },
  columns: [
    { data: "fecha" },
    { data: "detalle_canje" },
    { data: "vendedor" },
    { data: "canjeados" },                    
  ],
  columnDefs: [
    {
      targets: 0,
      render: DataTable.render.datetime('DD/MM/YYYY'),

                 
    },

    
  ],
  
  initComplete: function (settings, json) {
   var suma = $('#data').DataTable().column(4).data().sum();
   var suma1 = $('#data1').DataTable().column(3).data().sum();
   var resta = suma-suma1;
    $('#total1').html(suma1);
    $('#resta').html(resta);
   
  },
   
});


  
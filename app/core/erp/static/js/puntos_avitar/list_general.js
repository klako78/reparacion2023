$(function(){
     var table=$("#data").DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        order: [[3, 'desc']],
        ajax: {
          url: window.location.pathname,
          type: "POST",
          data: {
            action: "searchall",
           
          },
          dataSrc: "",
        },
        columns: [
          { data: "id" },
          { data: "cliente" },
          { data: "numero_factura" },
          { data: "total_factura" },
          { data: "vendedor" },
          { data: "fecha" },
          {"defaultContent": '<button type="button" class="eliminar btn-danger " id="eliminar" name="eliminar"><i class="fas fa-trash"></i></button>'}         
        ],
        columnDefs: [
          {
            targets: 3,
            render: DataTable.render.number( '.', ',', 2,'$ ', ' ' ),
          },
          {
            targets: 5,
            render: DataTable.render.datetime('DD/MM/YYYY'),
                       
          },
          {
            targets: 0,
            visible: false,
                       
          },
          
        ],
        
        initComplete: function (settings, json) {
         var suma = $('#data').DataTable().column(1).data().sum();
          $('#total').html(suma);
          
         
        },
      
      });

      optener_novedad('#data tbody',table)
    }); 
    optener_novedad=function(tbody,table){
      $(tbody).on("click","button.eliminar",function(){
        var data=table.row($(this).parents("tr")).data();
        location.href='list_general/delete/'+data.id;
         
          
       
      });
    
    
      
       
    }
        
     
  
   



    


 

           
         
         $('#btnfiltrar').on('click', function(){
            $('input[name="action"]').val('fecha');
            $('#fechaModal').modal('show');
        
            
         });


         $('#enviar').on('click', function(){
           var inicio= $('input[name="inicio"]').val();
           var fin= $('input[name="fin"]').val();
                $.ajax({
                 url: window.location.pathname,
                 type: "POST",
                 data: {
                   action: "fecha",
                   init: inicio,
                   end:  fin,
                 },
                 dataType: "json",
                 
               })
                 .done(function (data) {
                  
                  $("#data").DataTable({
                    responsive: true,
                    autoWidth: false,
                    destroy: true,
                    deferRender: true,
                    order: [[3, 'desc']],
                    ajax: {
                      url: window.location.pathname,
                      type: "POST",
                      data: {
                        action: "fecha",
                       
                      },
                      dataSrc: "",
                    },
                    data: data,
                    columns: [
                      { data: "cliente" },
                      { data: "numero_factura" },
                      { data: "total_factura" },
                      { data: "vendedor" },
                      { data: "fecha" },
                      { data: "puntaje_total" },
                      { data: "canjeados" },              
                    ],
                    columnDefs: [
                      {
                        targets: 2,
                        render: DataTable.render.number( '.', ',', 2,'$ ', ' ' ),
                        targets: 4,
                        render: DataTable.render.datetime('DD/MM/YYYY'),
                                   
                      },
            
                      
                    ],
                    
                    initComplete: function (settings, json) {
                     var suma = $('#data').DataTable().column(1).data().sum();
                      $('#total').html(suma);
                      
                     
                    },
                 
                  });
                
                  
                 })
                 .fail(function (jqXHR, textStatus, errorThrown) {
                   alert(textStatus + ": " + errorThrown);
                 })
                 .always(function (data) {});
            
      
        
                
      $('#fechaModal').modal('hide');
      });
    

        
   
      

    
   
   

$('#enviar').on('click', function(){
  var inicio= $('input[name="inicio"]').val();
  var fin= $('input[name="fin"]').val();
  var table=$("#data").DataTable({
    responsive: true,
    autoWidth: false,
    destroy: true,
    deferRender: true,
    order: [[3, 'desc']],
    ajax: {
      url: window.location.pathname,
      type: "POST",
      data: {
        action: "fecha",
        init: inicio,
        end:  fin,
       
      },
      dataSrc: "",
      
    },
    columns: [
      { data: "cedula" },
      { data: "nombre" },
      { data: "totalf" },
      { data: "totalp" },
      { data: "totalc" },
      { data: "totald" },
      
      {"defaultContent": '<button type="button" class="btnver btn-primary btn" id="btnver" name="btnver">Ver</button>'
  },     
    ],
    columnDefs: [
      {
        targets: 2,
        render: DataTable.render.number( '.', ',', 2,'$ ', ' ' ),
       
                   
      },
      {
        targets: 0,
        visible: false,
      }         
      
    ],
    
    initComplete: function (settings, json) {
     var suma = $('#data').DataTable().column(2).data().sum().toLocaleString();
      $('#total').html(suma);
       
     
    },
 
  });
  
$('#fechaModal').modal('hide');

$('#data tbody').on("click","button.btnver",function(){
  var data=table.row($(this).parents("tr")).data();
  location.assign('list/'+data.cedula);
});
});
        

      

    
   
   

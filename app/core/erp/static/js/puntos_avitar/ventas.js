$(function(){
  var table=$("#data").DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        order: [[3, 'desc']],
        ajax: {
          url: window.location.pathname,
          type: "POST",
          data: {
            action: "searchall",
           
          },
          dataSrc: "",
        },
        columns: [
          { data: "cedula" },
          { data: "nombre" },
          { data: "totalf" },
          { data: "totalp" },
          { data: "totalc" },
          { data: "totald" },
          
          {"defaultContent": '<button type="button" class="btnver btn-primary btn" id="btnver" name="btnver"<i class="icon-eye-open"></i>Ver</button>'
      },     
        ],
        columnDefs: [
          {
            targets: 2,
            render: DataTable.render.number( '.', ',', 2,'$ ', ' ' ),
           
                       
          },
          {
            targets: 0,
            visible: false,
          }         
          
        ],
        
        initComplete: function (settings, json) {
         var suma = $('#data').DataTable().column(2).data().sum().toLocaleString();
          $('#total').html(suma);
           
         
        },
     
      });
    $('#enviar').on('click', function(){
      var inicio= $('input[name="inicio"]').val();
      var fin= $('input[name="fin"]').val();
      $('input[name="init"]').val(inicio);
      $('input[name="end"]').val(fin);
      action="fecha"
      $.ajax({
        url: window.location.pathname,
        type: "POST",
        data: {
          action: action,
          init: inicio,
          end:  fin,
         
        },
        dataType: "json",
        }).done(function(data) {
          var table = $('#data').DataTable();
          table.clear().rows.add(data).draw();
          $('#fechaModal').modal('hide');
      });
      
  

    
    });
  optener_novedad('#data tbody',table) 
           
});
   
optener_novedad=function(tbody,table){
   $(tbody).on("click","button.btnver",function(){
    var data=table.row($(this).parents("tr")).data();
    location.assign('list/'+data.cedula+'/'+data.nombre);
 });
  
}

    


 
